<?php
// src/AppBundle/Controller/UploadController.php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\BuildingPlayer;
use AppBundle\Entity\BuildingList;
use AppBundle\Entity\Player;
use AppBundle\Entity\BuildingLevelList;
use AppBundle\Entity\Img;
use AppBundle\Entity\ImgIcone;
use AppBundle\Entity\ImgMissile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UploadController extends Controller
{
    public function addImgBuildingAction()
    {
        $buildingLevelList = $this->getBuildinglevelListArray();
        return ($this->render('views/uploadImg.html.php', array('buildingLevelList' => $buildingLevelList)));
    }

    public function addImgMissileAction()
    {
        $missileLevelList = $this->getMissilelevelListArray();
        return ($this->render('views/uploadImgMissile.html.php', array('missileLevelList' => $missileLevelList)));
    }

    public function addImgMissileIconeAction()
    {
        $missileLevelList = $this->getMissilelevelListArray();
        return ($this->render('views/uploadImgMissileIcone.html.php', array('missileLevelList' => $missileLevelList)));
    }

    public function setImgValueBuildingAction($idImg, $nbrImgX, $nbrImgY, $speed, $nbrCase)
    {
        $img = $this->getImgObject($idImg);
            if (!isset($img))
               throw new NotFoundHttpException("Critical error 1 !");
        if (!is_numeric($nbrImgX) || !is_numeric($nbrImgY) || !is_numeric($speed) || !is_numeric($nbrCase))
            throw new NotFoundHttpException("Critical error 2!");
        if ($nbrImgX <= 0 || $nbrImgX > 100 || $nbrImgY <= 0 || $nbrImgY > 100 || $speed < 0 || $speed > 200 || $nbrCase <= 0 || $nbrCase > 20)
            throw new NotFoundHttpException("Value Bad Set !");
        $em = $this->getDoctrine()->getManager();
        $img->setNbrCase($nbrCase);
        $img->setSpeed($speed);
        $img->setNbrImgX($nbrImgX);
        $img->setNbrImgY($nbrImgY);
        $em->flush();
        return new Response("Img has been setted :)");
    }   

    public function setImgValueMissileAction($idImg, $nbrImgX, $nbrImgY, $speed, $nbrCase)
    {
        $img = $this->getImgMissileObject($idImg);
            if (!isset($img))
               throw new NotFoundHttpException("Critical error 1 !");
        if (!is_numeric($nbrImgX) || !is_numeric($nbrImgY) || !is_numeric($speed) || !is_numeric($nbrCase))
            throw new NotFoundHttpException("Critical error 2!");
        if ($nbrImgX <= 0 || $nbrImgX > 100 || $nbrImgY <= 0 || $nbrImgY > 100 || $speed < 0 || $speed > 200 || $nbrCase <= 0 || $nbrCase > 20)
            throw new NotFoundHttpException("Value Bad Set !");
        $em = $this->getDoctrine()->getManager();
        $img->setNbrCase($nbrCase);
        $img->setSpeed($speed);
        $img->setNbrImgX($nbrImgX);
        $img->setNbrImgY($nbrImgY);
        $em->flush();
        return new Response("Img has been setted :)");
    }  

    public function deleteFileBuildingAction(Request $request, $idImg, $imageName)
    {
        $img = $this->getImgObject($idImg);
            if (!isset($img))
               throw new NotFoundHttpException("Critical error !");
        $e = explode("_", $imageName);
        if (!isset($e[4]) || (isset($e[4]) && !is_numeric(substr($e[4], 0, -4))))
            throw new NotFoundHttpException("Critical error !");
        $n = substr($e[4], 0, -4);
        $uploadDir = $this->container->getParameter('upload_sprite_dir');
        $folder = $e[0]."_".$e[1]."_".$e[2]."_".$e[3];
        $file = $uploadDir."/".$folder."/".$imageName;
        $em = $this->getDoctrine()->getManager();
        $em->remove($img);
        $em->flush();
        // on supprime l'image si elle existe
        if (file_exists($file))
        {
            // on delete le file
            unlink($file); //DELETE !
            // on renamme tous les autres files dans le dossier
            $files1 = scandir($uploadDir."/".$folder, 1);
            $i = 1;
            $list = "";
            sort($files1, SORT_NATURAL | SORT_FLAG_CASE);
            foreach ($files1 as $key => $value)
            {
                if ($value == $imageName)
                    continue ;
               if (strlen($value) > 8) // bypass . and .. file
               {
                    $old = $uploadDir."/".$folder."/".$value;
                    $new = $old;
                    $tt = explode("_", $new);
                    $j = substr($tt[7], 0, -4);
                    $new = substr($new , 0, -4 - strlen($j)).($i).".png";
                    //echo $old. " ===> ". $new."<br />";
                    rename ($old, $new);
                    $tt = explode("_", $new);
                    $list .= "SPRITE_".$tt[4]."_".$tt[5]."_".$tt[6]."_".$tt[7].";";
                    $i++;
               }
            }
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($img);
            $em->flush();
            throw new NotFoundHttpException("Critical error !"); 
        }
        return new Response("ok");
    }

    public function deleteFileMissileAction(Request $request, $idImg, $imageName)
    {
        $img = $this->getImgMissileObject($idImg);
            if (!isset($img))
               throw new NotFoundHttpException("Critical error !");
        $e = explode("_", $imageName);
        if (!isset($e[4]) || (isset($e[4]) && !is_numeric(substr($e[4], 0, -4))))
            throw new NotFoundHttpException("Critical error !");
        $n = substr($e[4], 0, -4);
        $uploadDir = $this->container->getParameter('upload_sprite_dir');
        $folder = $e[0]."_".$e[1]."_".$e[2]."_".$e[3];
        $file = $uploadDir."/".$folder."/".$imageName;
        $em = $this->getDoctrine()->getManager();
        $em->remove($img);
        $em->flush();
        // on supprime l'image si elle existe
        if (file_exists($file))
        {
            // on delete le file
            unlink($file); //DELETE !
            // on renamme tous les autres files dans le dossier
            $files1 = scandir($uploadDir."/".$folder, 1);
            $i = 1;
            $list = "";
            sort($files1, SORT_NATURAL | SORT_FLAG_CASE);
            foreach ($files1 as $key => $value)
            {
                if ($value == $imageName)
                    continue ;
               if (strlen($value) > 8) // bypass . and .. file
               {
                    $old = $uploadDir."/".$folder."/".$value;
                    $new = $old;
                    $tt = explode("_", $new);
                    $j = substr($tt[7], 0, -4);
                    $new = substr($new , 0, -4 - strlen($j)).($i).".png";
                    //echo $old. " ===> ". $new."<br />";
                    rename ($old, $new);
                    $tt = explode("_", $new);
                    $list .= "SPRITE_".$tt[4]."_".$tt[5]."_".$tt[6]."_".$tt[7].";";
                    $i++;
               }
            }
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($img);
            $em->flush();
            throw new NotFoundHttpException("Critical error !"); 
        }
        return new Response("ok");
    }

    public function deleteFileMissileIconeAction(Request $request, $idImg, $imageName)
    {
        $img = $this->getImgMissileIconeObject($idImg);
            if (!isset($img))
               throw new NotFoundHttpException("Critical error !");
        $e = explode("_", $imageName);
        if (!isset($e[4]) || (isset($e[4]) && !is_numeric(substr($e[4], 0, -4))))
            throw new NotFoundHttpException("Critical error !");
        $n = substr($e[4], 0, -4);
        $uploadDir = $this->container->getParameter('upload_sprite_dir');
        $folder = $e[0]."_".$e[1]."_".$e[2]."_".$e[3];
        $file = $uploadDir."/".$folder."/".$imageName;
        $em = $this->getDoctrine()->getManager();
        $em->remove($img);
        $em->flush();
        // on supprime l'image si elle existe
        if (file_exists($file))
        {
            // on delete le file
            unlink($file); //DELETE !
            // on renamme tous les autres files dans le dossier
            $files1 = scandir($uploadDir."/".$folder, 1);
            $i = 1;
            $list = "";
            sort($files1, SORT_NATURAL | SORT_FLAG_CASE);
            foreach ($files1 as $key => $value)
            {
                if ($value == $imageName)
                    continue ;
               if (strlen($value) > 8) // bypass . and .. file
               {
                    $old = $uploadDir."/".$folder."/".$value;
                    $new = $old;
                    $tt = explode("_", $new);
                    $j = substr($tt[7], 0, -4);
                    $new = substr($new , 0, -4 - strlen($j)).($i).".png";
                    //echo $old. " ===> ". $new."<br />";
                    rename ($old, $new);
                    $tt = explode("_", $new);
                    $list .= "SPRITE_".$tt[4]."_".$tt[5]."_".$tt[6]."_".$tt[7].";";
                    $i++;
               }
            }
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($img);
            $em->flush();
            throw new NotFoundHttpException("Critical error !"); 
        }
        return new Response("ok");
    }

    public function editFileBuildingAction(Request $request, $idImg, $imageName)
    {
        if (!isset($_FILES['file']))
            return ;
        if (0 < $_FILES['file']['error'] )
            return new Response('Error: ' . $_FILES['file']['error'] . '<br>');
        else
        {
           if (!$this->is_image($_FILES['file']['tmp_name']))
              throw new NotFoundHttpException("File is not an image !");
            $img = $this->getImgObject($idImg);
            if (!isset($img))
               throw new NotFoundHttpException("Critical error !");
            $uploadDir = $this->container->getParameter('upload_sprite_dir');
            $e = explode("_", $imageName);
            $folder = $e[0]."_".$e[1]."_".$e[2]."_".$e[3];
            print($uploadDir."/".$folder."/".$imageName);
            move_uploaded_file($_FILES['file']['tmp_name'], $uploadDir."/".$folder."/".$imageName);
            $em = $this->getDoctrine()->getManager();
            $img->setRandomId(mt_rand(1000000, 100000000));
            $em->flush();
            return new Response(" have been Upload :)");
      }
    }

    public function editFileMissileAction(Request $request, $idImg, $imageName)
    {
        if (!isset($_FILES['file']))
            return ;
        if (0 < $_FILES['file']['error'] )
            return new Response('Error: ' . $_FILES['file']['error'] . '<br>');
        else
        {
           if (!$this->is_image($_FILES['file']['tmp_name']))
              throw new NotFoundHttpException("File is not an image !");
            $img = $this->getImgMissileObject($idImg);
            if (!isset($img))
               throw new NotFoundHttpException("Critical error !");
            $uploadDir = $this->container->getParameter('upload_sprite_dir');
            $e = explode("_", $imageName);
            $folder = $e[0]."_".$e[1]."_".$e[2]."_".$e[3];
            print($uploadDir."/".$folder."/".$imageName);
            move_uploaded_file($_FILES['file']['tmp_name'], $uploadDir."/".$folder."/".$imageName);
            $em = $this->getDoctrine()->getManager();
            $img->setRandomId(mt_rand(1000000, 100000000));
            $em->flush();
            return new Response(" have been Upload :)");
      }
    }

    public function uploadFileBuildingAction(Request $request)
    {
        print_r($_FILES);
        if (!empty($_FILES) && isset($_POST['buildingLevelListForm']))
        {
            $count = count($_FILES['file']['name']);
            $em = $this->getDoctrine()->getManager();
            for ($i = 0; $i < $count; $i++)
            {
                if (!$this->is_image($_FILES['file']['tmp_name'][$i]))
                    return new Response("File is not an image !");
                $b = $this->getBuildinglevelListObject($_POST['buildingLevelListForm']);
                if (!isset($b))
                   return new Response("Critical error !");
                $uploadDir = $this->container->getParameter('upload_sprite_dir');
                $name = $this->findSpriteBuildingName($b, $uploadDir, $i);
                $folder = $this->findFolderName($_POST['buildingLevelListForm'], $b, $uploadDir);
                move_uploaded_file($_FILES['file']['tmp_name'][$i], $folder."/".$name);
                $img = new Img();
                $img->setName($name);
                $img->setRandomId(mt_rand(1000000, 100000000));
                $img->setNbrImgX(0);
                $img->setNbrImgY(0);
                $img->setNbrCase(0);
                $img->setSpeed(0);
                $img->setBuildingLevelList($b);
                $em->persist($img);
            }
            $em->flush();
            return new Response($count."Files Upload :)");
        }
        else
        {
            if (empty($FILES))
               return new Response("empty"); 
             return new Response("Error, waiting for action");
        }
    }

    public function uploadFileMissileAction(Request $request)
    {
        print_r($_FILES);
        if (!empty($_FILES) && isset($_POST['missileLevelListForm']))
        {
            $count = count($_FILES['file']['name']);
            $em = $this->getDoctrine()->getManager();
            for ($i = 0; $i < $count; $i++)
            {
                if (!$this->is_image($_FILES['file']['tmp_name'][$i]))
                    return new Response("File is not an image !");
                $b = $this->getMissilelevelListObject($_POST['missileLevelListForm']);
                if (!isset($b))
                   return new Response("Critical error !");
                $uploadDir = $this->container->getParameter('upload_sprite_dir');
                $name = $this->findSpriteMissileName($b, $uploadDir, $i);
                $folder = $this->findFolderMissileName($_POST['missileLevelListForm'], $b, $uploadDir);
                move_uploaded_file($_FILES['file']['tmp_name'][$i], $folder."/".$name);
                $img = new ImgMissile();
                $img->setName($name);
                $img->setRandomId(mt_rand(1000000, 100000000));
                $img->setNbrImgX(0);
                $img->setNbrImgY(0);
                $img->setNbrCase(0);
                $img->setSpeed(0);
                $img->setMissileLevelList($b);
                $em->persist($img);
            }
            $em->flush();
            return new Response($count."Files Upload :)");
        }
        else
        {
            if (empty($FILES))
               return new Response("empty"); 
             return new Response("Error, waiting for action");
        }
    }

    public function uploadFileMissileIconeAction(Request $request)
    {
        print_r($_FILES);
        if (!empty($_FILES) && isset($_POST['missileLevelListForm']))
        {
            $count = count($_FILES['file']['name']);
            $em = $this->getDoctrine()->getManager();
            for ($i = 0; $i < $count; $i++)
            {
                if (!$this->is_image($_FILES['file']['tmp_name'][$i]))
                    return new Response("File is not an image !");
                $b = $this->getMissilelevelListObject($_POST['missileLevelListForm']);
                if (!isset($b))
                   return new Response("Critical error !");
                $uploadDir = $this->container->getParameter('upload_sprite_dir');
                $name = $this->findSpriteMissileIconeName($b, $uploadDir, $i);
                $folder = $this->findFolderMissileIconeName($_POST['missileLevelListForm'], $b, $uploadDir);
                move_uploaded_file($_FILES['file']['tmp_name'][$i], $folder."/".$name);
                $img = new ImgIcone();
                $img->setName($name);
                $img->setRandomId(mt_rand(1000000, 100000000));
                $img->setMissileLevelList($b);
                $em->persist($img);
            }
            $em->flush();
            return new Response($count."Files Upload :)");
        }
        else
        {
            if (empty($FILES))
               return new Response("empty"); 
             return new Response("Error, waiting for action");
        }
    }

    public function findFolderName($buildingLevelListId, $b, $dir)
    {
        $folderDir = $dir."/"."SPRITE_BUILDING_".$b->getBuildingList()->getName()."_".$b->getLevel();
        if (!is_dir($folderDir))
        {
            mkdir($folderDir, 0777);
        }
        return ($folderDir);
    }

    public function findFolderMissileName($missileLevelListId, $b, $dir)
    {
        $folderDir = $dir."/"."SPRITE_MISSILE_".$b->getMissileList()->getName()."_".$b->getMissileLevel();
        if (!is_dir($folderDir))
        {
            mkdir($folderDir, 0777);
        }
        return ($folderDir);
    }

    public function findFolderMissileIconeName($missileLevelListId, $b, $dir)
    {
        $folderDir = $dir."/"."SPRITE_MISSILEICONE_".$b->getMissileList()->getName()."_".$b->getMissileLevel();
        if (!is_dir($folderDir))
        {
            mkdir($folderDir, 0777);
        }
        return ($folderDir);
    }

    public function findSpriteBuildingName($b, $dir, $i)
    {
        $img_db = $b->getImg();
        $name = "SPRITE_BUILDING_".$b->getBuildingList()->getName()."_".$b->getLevel()."_".(count($img_db) + 1 + $i).".png";
        return ($name);
    }

    public function findSpriteMissileName($b, $dir, $i)
    {
        $img_db = $b->getImg();
        $name = "SPRITE_MISSILE_".$b->getMissileList()->getName()."_".$b->getMissileLevel()."_".(count($img_db) + 1 + $i).".png";
        return ($name);
    }

    public function findSpriteMissileIconeName($b, $dir, $i)
    {
        $img_db = $b->getImgIcone();
        $name = "SPRITE_MISSILEICONE_".$b->getMissileList()->getName()."_".$b->getMissileLevel()."_".(count($img_db) + 1 + $i).".png";
        return ($name);
    }

    public function getBuildinglevelListArray()
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:BuildingLevelList');
        $b = $respository->findAll();
        return ($b);
    }

    public function getMissilelevelListArray()
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:MissileLevelList');
        $b = $respository->findAll();
        return ($b);
    }

    public function getBuildinglevelListObject($buildingLevelListId)
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:BuildingLevelList');
        $b = $respository->find($buildingLevelListId);
        return ($b);
    }

    public function getMissilelevelListObject($missileLevelListId)
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:MissileLevelList');
        $b = $respository->find($missileLevelListId);
        return ($b);
    }

    public function getImgObject($imgId)
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:Img');
        $b = $respository->find($imgId);
        return ($b);
    }

    public function getImgMissileObject($imgId)
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:ImgMissile');
        $b = $respository->find($imgId);
        return ($b);
    }

    public function getImgMissileIconeObject($imgId)
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:ImgIcone');
        $b = $respository->find($imgId);
        return ($b);
    }

    function is_image($path)
    {
        $a = getimagesize($path);
        $image_type = $a[2];
        if (in_array($image_type , array(IMAGETYPE_GIF, IMAGETYPE_JPEG ,IMAGETYPE_PNG ,IMAGETYPE_BMP)))
            return true;
        return false;
    }
}
