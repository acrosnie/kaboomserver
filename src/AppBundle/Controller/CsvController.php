<?php
// src/AppBundle/Controller/CsvController.php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\BuildingPlayer;
use AppBundle\Entity\BuildingList;
use AppBundle\Entity\BuildingEffect;
use AppBundle\Entity\player;
use AppBundle\Entity\BuildingLevelList;
use AppBundle\Entity\Img;
use AppBundle\Entity\MissileList;
use AppBundle\Entity\MissileLevelList;
use AppBundle\Entity\MissilePlayer;

class CsvController extends Controller
{

    function __CsvController()
    {

    }

    public function is_csv($name)
    {
        $a = explode(".", $name);
        if ($a[count($a) - 1] == "csv")
            return (true);
        return (false);
    }

    public function save_csv($nameFiles, $nameCsv)
    {
        $storagename = $nameCsv;
        $dest = $this->container->getParameter('kernel.root_dir').'/../web/csv/'.$storagename;
        move_uploaded_file($_FILES[$nameFiles]["tmp_name"], $dest);
    }

    public function read_and_dsiplay_csv($name)
    {
        $row = 1;
        $csv = $this->container->getParameter('kernel.root_dir').'/../web/csv/'.$name;
        if (($handle = fopen($csv, "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                $num = count($data);
                echo "<p> $num champs à la ligne $row: <br /></p>\n";
                $row++;
                for ($c=0; $c < $num; $c++)
                {
                    echo $data[$c] . "<br />\n";
                }
            }
            fclose($handle);
        }
    }

    //---------------------------------------------------------------------------------------------------------------- csv_level_building_max
    public function do_csv_level_building_max($name)
    {
        $array = array();
        $row = 1;
        $csv = $this->container->getParameter('kernel.root_dir').'/../web/csv/'.$name;
        if (($handle = fopen($csv, "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                if ($row == 1)
                {
                    if (count($data) >= 6 && $data[0] == "" && $data[1] == "1" && $data[2] == "2" && $data[3] == "3" && $data[4] == "4" && $data[5] == "5")
                    {
                        $row++;
                        continue ;
                    }
                    else
                        return (null);
                }
                else if (count($data) >= 6)
                {
                    if (!isset($array[$data[0]]))
                        $array[$data[0]] = "";
                    $array[$data[0]] .= "1:".$data[1].",2:".$data[2].",3:".$data[3].",4:".$data[4].",5:".$data[5];
                }
                else
                    return (null);
                $row++;
            }
            fclose($handle);
        }
        // foreach ($array as $key => $value)
        // {
        //     $array[$key] = substr($value, 0, -1);
        // }
        return ($array);
    }

    public function csvLevelBuildingMaxAction(Request $request, $success, $error)
    {
        if ($request->getMethod() == 'POST' && isset($_POST['csvPost']))
        {
            if (isset($_FILES['csvFile']) && $this->is_csv($_FILES['csvFile']['name']))
            {
                $this->save_csv('csvFile', 'csvLevelBuildingMax.csv');
                $a = $this->do_csv_level_building_max('csvLevelBuildingMax.csv');
                if ($a == null)
                    return $this->redirect($this->generateUrl('csv_level_building_max', array("error" => true)));
                $em = $this->getDoctrine()->getManager();
                foreach ($a as $key => $value)
                {
                    //update database
                    $building = $em->getRepository('AppBundle:BuildingList')->findByName($key);
                    if (!isset($building[0]))
                        return $this->redirect($this->generateUrl('csv_level_building_max', array("error" => true)));
                    $building[0]->setLevelNbr($value);
                }
                $em->flush();
                return $this->redirect($this->generateUrl('csv_level_building_max', array("success" => true)));
            }
            else
                return $this->redirect($this->generateUrl('csv_level_building_max', array("error" => true)));
            return (null);
        }
        return ($this->render('views/csvLevelBuildingMaxAction.html.php', array(
        'success' => $success, 'error' => $error,
        )));
    }

    //---------------------------------------------------------------------------------------------------------------- csv_building_level_List_info

    public function do_csv_building_level_list_info($name)
    {
        $repository1 = $this->getDoctrine()->getRepository('AppBundle:BuildingList');
        $repository2 = $this->getDoctrine()->getRepository('AppBundle:BuildingEffect');
        $row = 1;
        $arrayCsv = array();
        $arrayCsvChamp = array();
        $csv = $this->container->getParameter('kernel.root_dir').'/../web/csv/'.$name;
        if (($handle = fopen($csv, "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                $num = count($data);
                for ($c=0; $c < $num; $c++)
                {
                    if ($row == 1)
                    {
                        if (count($data) >= 22 && $data[0] == "Name" && $data[1] == "Level" && $data[2] == "Health" && $data[3] == "CostRessource1Build" && $data[4] == "CostRessource2Build" && $data[5] == "CostRessource3Build" && $data[6] == "CostRessource1Upgrade" && $data[7] == "CostRessource2Upgrade" && $data[8] == "CostRessource3Upgrade" && $data[9] == "TimeUpgrade" && $data[10] == "CapacityRessource1"
                            && $data[11] == "CapacityRessource2" && $data[12] == "CapacityRessource3" && $data[13] == "StockRessource1" && $data[14] == "StockRessource2" && $data[15] == "StockRessource3" && $data[16] == "ProdRateRessource1" && $data[17] == "ProdRateRessource2" && $data[18] == "ProdRateRessource3" && $data[19] == "Buyable" && $data[20] == "PreConstructed" && $data[21] == "Effect")
                        {
                            $arrayCsvChamp[$c] = $data[$c]; // on cree l'array
                            if ($data[$c] == "Name")
                                $arrayCsvChamp[$c] = "IdBuilding";
                            else if ($data[$c] == "Effect")
                                $arrayCsvChamp[$c] = "IdEffect";
                        }
                        else
                            return (null);
                    }
                    else
                    {
                        $arrayCsv[$row][$arrayCsvChamp[$c]] = $data[$c];
                        if ($arrayCsvChamp[$c] == "IdBuilding")
                        {   
                            $b = $repository1->findOneByName($data[$c]);
                            if (!isset($b))
                                return (null);
                            $arrayCsv[$row][$arrayCsvChamp[$c]] = $b->getId();
                        }
                        else if ($arrayCsvChamp[$c] == "IdEffect")
                        {   
                            $b = $repository2->findOneByEffect($data[$c]);
                            if (!isset($b))
                            {
                                $arrayCsv[$row][$arrayCsvChamp[$c]] = null;
                            }
                            else
                                $arrayCsv[$row][$arrayCsvChamp[$c]] = $b->getId();
                        }
                    }
                }
                $row++;
            }
        }
        else
            return (null);
        return ($arrayCsv);
    }

    public function csvBuildingLevelListInfoAction(Request $request, $success, $error)
    {
        if ($request->getMethod() == 'POST' && isset($_POST['csvPost']))
        {
            if (isset($_FILES['csvFile']) && $this->is_csv($_FILES['csvFile']['name']))
            {
                $repository = $this->getDoctrine()->getRepository('AppBundle:BuildingLevelList');
                $this->save_csv('csvFile', 'csvBuildingLevelListInfo.csv');
                $a = $this->do_csv_building_level_list_info('csvBuildingLevelListInfo.csv');
                if ($a == null)
                    return $this->redirect($this->generateUrl('csv_building_level_list_info', array("error" => true)));
                $em = $this->getDoctrine()->getManager();
                foreach ($a as $key => $value)
                {
                    $entry = $repository->findOneBy(
                        array('buildingList' => $value['IdBuilding'], 'level' => $value['Level'])
                    );
                    if (isset($entry))
                    {
                        if ($value['IdEffect'] != null)
                        {
                            $effect = $this->getDoctrine()
                                    ->getRepository('AppBundle:BuildingEffect')
                                    ->find($value['IdEffect']);
                            if (isset ($effect))
                                $entry->setBuildingEffect($effect);
                        }
                        $entry->setHealth($value['Health']);
                        $entry->setCostRessource1Build($value['CostRessource1Build']);
                        $entry->setCostRessource2Build($value['CostRessource2Build']);
                        $entry->setCostRessource3Build($value['CostRessource3Build']);
                        $entry->setCostRessource1Upgrade($value['CostRessource1Upgrade']);
                        $entry->setCostRessource2Upgrade($value['CostRessource2Upgrade']);
                        $entry->setCostRessource3Upgrade($value['CostRessource3Upgrade']);
                        $entry->setTime($value['TimeUpgrade']);
                        $entry->setStockRessource1($value['StockRessource1']);
                        $entry->setStockRessource2($value['StockRessource2']);
                        $entry->setStockRessource3($value['StockRessource3']);
                        $entry->setCapacityRessource1($value['CapacityRessource1']);
                        $entry->setCapacityRessource2($value['CapacityRessource2']);
                        $entry->setCapacityRessource3($value['CapacityRessource3']);
                        $entry->setProdRateRessource1($value['ProdRateRessource1']);
                        $entry->setProdRateRessource2($value['ProdRateRessource2']);
                        $entry->setProdRateRessource3($value['ProdRateRessource3']);
                        $buyable = (strtolower($value['Buyable']) == "true") ? 1 : 0;
                        $preConstructed = (strtolower($value['PreConstructed']) == "true") ? 1 : 0;
                        $entry->setBuyable($buyable);
                        $entry->setPreConstructed($preConstructed);
                    }
                    else
                    {
                        $b = $this->getDoctrine()
                                ->getRepository('AppBundle:BuildingList')
                                ->find($value['IdBuilding']);
                        if ($value['IdEffect'] != null)
                        {
                            $effect = $this->getDoctrine()
                                    ->getRepository('AppBundle:BuildingEffect')
                                    ->find($value['IdEffect']);
                        }
                        $building = new BuildingLevelList();
                        $building->setBuildingList($b);
                        if (isset ($effect))
                            $building->setBuildingEffect($effect);
                        $building->setLevel($value['Level']);
                        $building->setHealth($value['Health']);
                        $building->setCostRessource1Build($value['CostRessource1Build']);
                        $building->setCostRessource2Build($value['CostRessource2Build']);
                        $building->setCostRessource3Build($value['CostRessource3Build']);
                        $building->setCostRessource1Upgrade($value['CostRessource1Upgrade']);
                        $building->setCostRessource2Upgrade($value['CostRessource2Upgrade']);
                        $building->setCostRessource3Upgrade($value['CostRessource3Upgrade']);
                        $building->setTime($value['TimeUpgrade']);
                        $building->setStockRessource1($value['StockRessource1']);
                        $building->setStockRessource2($value['StockRessource2']);
                        $building->setStockRessource3($value['StockRessource3']);
                        $building->setCapacityRessource1($value['CapacityRessource1']);
                        $building->setCapacityRessource2($value['CapacityRessource2']);
                        $building->setCapacityRessource3($value['CapacityRessource3']);
                        $building->setProdRateRessource1($value['ProdRateRessource1']);
                        $building->setProdRateRessource2($value['ProdRateRessource2']);
                        $building->setProdRateRessource3($value['ProdRateRessource3']);
                        $building->setBuyable($value['Buyable']);
                        $building->setPreConstructed($value['PreConstructed']);
                        $em->persist($building);
                    }
                }
                $em->flush();
                return $this->redirect($this->generateUrl('csv_building_level_list_info', array("success" => true)));
                return (null);
            }
            else
               return $this->redirect($this->generateUrl('csv_building_level_list_info', array("error" => true))); 
        }
        return ($this->render('views/csvBuildingLevelListInfoAction.html.php', array(
        'success' => $success, 'error' => $error,
        )));
    }

    //---------------------------------------------------------------------------------------------------------------- csv_building_list

    public function csvBuildingListAction(Request $request, $success, $error)
    {
        if ($request->getMethod() == 'POST' && isset($_POST['csvPost']))
        {
            if (isset($_FILES['csvFile']) && $this->is_csv($_FILES['csvFile']['name']))
            {
                $repository = $this->getDoctrine()->getRepository('AppBundle:BuildingList');
                $this->save_csv('csvFile', 'csvBuildingList.csv');
                $a = $this->do_csv_building_list('csvBuildingList.csv');
                if ($a == null)
                    return $this->redirect($this->generateUrl('csv_building_list', array("error" => true)));
                $em = $this->getDoctrine()->getManager();
                $b = $repository->findAll();
                foreach ($b as $key => $value)
                {
                    $em->remove($value);
                }
                foreach ($a as $key => $value)
                {
                    $building = new BuildingList();
                    $building->setName($value['Name']);
                    $building->setLevelNbr("");
                    $em->persist($building);
                }
                $em->flush();
                return $this->redirect($this->generateUrl('csv_building_list', array("success" => true)));
            }
            else
               return $this->redirect($this->generateUrl('csv_building_list', array("error" => true))); 
        }
        return ($this->render('views/csvBuildingList.html.php', array(
        'success' => $success, 'error' => $error,
        )));
    }

    public function do_csv_building_list($name)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:BuildingList');
        $row = 1;
        $arrayCsv = array();
        $arrayCsvChamp = array();
        $csv = $this->container->getParameter('kernel.root_dir').'/../web/csv/'.$name;
        if (($handle = fopen($csv, "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                $num = count($data);
                for ($c=0; $c < $num; $c++)
                {
                    if ($row == 1)
                    {
                        if (count($data) == 1 && $data[0] == "Name")
                            $arrayCsvChamp[$c] = $data[$c]; // on cree l'array
                        else
                            return (null);
                    }
                    else
                        $arrayCsv[$row][$arrayCsvChamp[$c]] = $data[$c];
                }
                $row++;
            }
        }
        else
            return (null);
        return ($arrayCsv);
    }

    //---------------------------------------------------------------------------------------------------------------- csv_building_effect_list

    public function csvBuildingEffectListAction(Request $request, $success, $error)
    {
        if ($request->getMethod() == 'POST' && isset($_POST['csvPost']))
        {
            if (isset($_FILES['csvFile']) && $this->is_csv($_FILES['csvFile']['name']))
            {
                $repository = $this->getDoctrine()->getRepository('AppBundle:BuildingEffect');
                $this->save_csv('csvFile', 'csvBuildingEffectList.csv');
                $a = $this->do_csv_building_effect_list('csvBuildingEffectList.csv');
                if ($a == null)
                    return $this->redirect($this->generateUrl('csv_building_effect_list', array("error" => true)));
                $em = $this->getDoctrine()->getManager();
                $b = $repository->findAll();
                foreach ($b as $key => $value)
                {
                    $em->remove($value);
                }
                foreach ($a as $key => $value)
                {
                    $buildingEffect = new BuildingEffect();
                    $buildingEffect->setEffect($value['Effect']);
                    $buildingEffect->setNumberAvailable($value['NumberAvailable']);
                    $buildingEffect->setCooldown($value['Cooldown']);
                    $buildingEffect->setHealthAbility($value['HealthAbility']);
                    $buildingEffect->setDuration($value['Duration']);
                    $em->persist($buildingEffect);
                }
                $em->flush();
                return $this->redirect($this->generateUrl('csv_building_effect_list', array("success" => true)));
            }
            else
               return $this->redirect($this->generateUrl('csv_building_effect_list', array("error" => true))); 
        }
        return ($this->render('views/csvBuildingEffectList.html.php', array(
        'success' => $success, 'error' => $error,
        )));
    }

    public function do_csv_building_effect_list($name)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:BuildingEffect');
        $row = 1;
        $arrayCsv = array();
        $arrayCsvChamp = array();
        $csv = $this->container->getParameter('kernel.root_dir').'/../web/csv/'.$name;
        if (($handle = fopen($csv, "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                $num = count($data);
                for ($c=0; $c < $num; $c++)
                {
                    if ($row == 1)
                    {
                        if (count($data) == 5 && $data[0] == "Effect" && $data[1] == "NumberAvailable" && $data[2] == "Cooldown" && $data[3] == "HealthAbility" && $data[4] == "Duration")
                            $arrayCsvChamp[$c] = $data[$c]; // on cree l'array
                        else
                            return (null);
                    }
                    else
                        $arrayCsv[$row][$arrayCsvChamp[$c]] = $data[$c];
                }
                $row++;
            }
        }
        else
            return (null);
        return ($arrayCsv);
    }

    //---------------------------------------------------------------------------------------------------------------- csv_missile_list

    public function csvMissileListAction(Request $request, $success, $error)
    {
        if ($request->getMethod() == 'POST' && isset($_POST['csvPost']))
        {
            if (isset($_FILES['csvFile']) && $this->is_csv($_FILES['csvFile']['name']))
            {
                $repository = $this->getDoctrine()->getRepository('AppBundle:MissileList');
                $this->save_csv('csvFile', 'csvMissileList.csv');
                $a = $this->do_csv_missile_list('csvMissileList.csv');
                if ($a == null)
                    return $this->redirect($this->generateUrl('csv_missile_list', array("error" => true)));
                $em = $this->getDoctrine()->getManager();
                $b = $repository->findAll();
                foreach ($b as $key => $value)
                {
                    $em->remove($value);
                }
                foreach ($a as $key => $value)
                {
                    $missile = new MissileList();
                    $missile->setName($value['Name']);
                    $missile->setLvlMissileFact($value['LvlMissileFact']);
                    $missile->setDescription($value['Description']);
                    $missile->setAtt($value['Attack']);
                    $missile->setDef($value['Defense']);
                    $em->persist($missile);
                }
                $em->flush();
                return $this->redirect($this->generateUrl('csv_missile_list', array("success" => true)));
            }
            else
               return $this->redirect($this->generateUrl('csv_missile_list', array("error" => true))); 
        }
        return ($this->render('views/csvMissileList.html.php', array(
        'success' => $success, 'error' => $error,
        )));
    }

    public function do_csv_missile_list($name)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:MissileList');
        $row = 1;
        $arrayCsv = array();
        $arrayCsvChamp = array();
        $csv = $this->container->getParameter('kernel.root_dir').'/../web/csv/'.$name;
        if (($handle = fopen($csv, "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                $num = count($data);
                for ($c=0; $c < $num; $c++)
                {
                    if ($row == 1)
                    {
                        if (count($data) == 5 && $data[0] == "Name" && $data[1] == "LvlMissileFact" && $data[2] == "Description" && $data[3] == "Attack" && $data[4] == "Defense")
                            $arrayCsvChamp[$c] = $data[$c]; // on cree l'array
                        else
                            return (null);
                    }
                    else
                        $arrayCsv[$row][$arrayCsvChamp[$c]] = $data[$c];
                }
                $row++;
            }
        }
        else
            return (null);
        return ($arrayCsv);
    }

    //---------------------------------------------------------------------------------------------------------------- csv_missile_level_List_info

    public function do_csv_missile_level_list_info($name)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:MissileList');
        $row = 1;
        $arrayCsv = array();
        $arrayCsvChamp = array();
        $csv = $this->container->getParameter('kernel.root_dir').'/../web/csv/'.$name;
        if (($handle = fopen($csv, "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
            {
                $num = count($data);
                for ($c=0; $c < $num; $c++)
                {
                    if ($row == 1)
                    {
                        if (count($data) >= 14 && $data[0] == "Name" && $data[1] == "MissileLvl" && $data[2] == "LabUnlockLvl" && $data[3] == "CostRessource1" && $data[4] == "CostRessource2" && $data[5] == "CostRessource3"
                            && $data[6] == "ProdTime" && $data[7] == "CoolDown" && $data[8] == "Health" && $data[9] == "DmgMissiles" && $data[10] == "DmgBuildings" && $data[11] == "Speed" && $data[12] == "Area"
                            && $data[13] == "TimeExplosion" && $data[14] == "Freeze" && $data[15] == "SpeedReduc" && $data[16] == "Magnet" && $data[17] == "AttractionRadius" && $data[18] == "Draggable")
                        {
                            $arrayCsvChamp[$c] = $data[$c]; // on cree l'array
                            if ($data[$c] == "Name")
                                $arrayCsvChamp[$c] = "IdMissile"; 
                        }
                        else
                        {
                            return (null);
                        }
                    }
                    else
                    {
                        $arrayCsv[$row][$arrayCsvChamp[$c]] = $data[$c];
                        if ($arrayCsvChamp[$c] == "IdMissile")
                        {   
                            $b = $repository->findOneByName($data[$c]);
                            if (!isset($b))
                            {
                                return (null);
                            }
                            $arrayCsv[$row][$arrayCsvChamp[$c]] = $b->getId();
                        }
                    }
                }
                $row++;
            }
        }
        else
            return (null);
        return ($arrayCsv);
    }

    public function csvMissileLevelListInfoAction(Request $request, $success, $error)
    {
        if ($request->getMethod() == 'POST' && isset($_POST['csvPost']))
        {
            if (isset($_FILES['csvFile']) && $this->is_csv($_FILES['csvFile']['name']))
            {
                $repository = $this->getDoctrine()->getRepository('AppBundle:MissileLevelList');
                $this->save_csv('csvFile', 'csvMissileLevelListInfo.csv');
                $a = $this->do_csv_missile_level_list_info('csvMissileLevelListInfo.csv');
                if ($a == null)
                    return $this->redirect($this->generateUrl('csv_missile_level_list_info', array("error" => true)));
                $em = $this->getDoctrine()->getManager();
                foreach ($a as $key => $value)
                {
                    $entry = $repository->findOneBy(
                        array('missileList' => $value['IdMissile'], 'missileLevel' => $value['MissileLvl'])
                    );
                    if (isset($entry))
                    {
                        $entry->setDamageMissile($value['DmgMissiles']);
                        $entry->setDamageBuilding($value['DmgBuildings']);
                        $entry->setHealth($value['Health']);
                        $entry->setMissileLevel($value['MissileLvl']);
                        $entry->setLabUnlockLevel($value['LabUnlockLvl']);
                        $entry->setRessource1Cost($value['CostRessource1']);
                        $entry->setRessource2Cost($value['CostRessource2']);
                        $entry->setRessource3Cost($value['CostRessource3']);
                        $entry->setAttractionRadius($value['AttractionRadius']);
                        $entry->setArea(str_replace(',', '.', $value['Area']));
                        $entry->setSpeed(str_replace(',', '.', $value['Speed']));
                        $entry->setTime(str_replace(',', '.', $value['ProdTime']));
                        $entry->setTimeExplosion(str_replace(',', '.', $value['TimeExplosion']));
                        $entry->setCoolDown(str_replace(',', '.', $value['CoolDown']));
                        $entry->setSpeedReduc(str_replace(',', '.', $value['SpeedReduc']));
                        $entry->setFreeze(($value['Freeze'] == "True") ? 1 : 0);
                        $entry->setMagnet(($value['Magnet'] == "True") ? 1 : 0);
                        $entry->setDraggable(($value['Draggable'] == "True") ? 1 : 0);
                    }
                    else
                    {
                        $b = $this->getDoctrine()
                                ->getRepository('AppBundle:MissileList')
                                ->find($value['IdMissile']);
                        $missile = new MissileLevelList();
                        $missile->setMissileList($b);
                        $missile->setDamageMissile($value['DmgMissiles']);
                        $missile->setDamageBuilding($value['DmgBuildings']);
                        $missile->setHealth($value['Health']);
                        $missile->setMissileLevel($value['MissileLvl']);
                        $missile->setLabUnlockLevel($value['LabUnlockLvl']);
                        $missile->setRessource1Cost($value['CostRessource1']);
                        $missile->setRessource2Cost($value['CostRessource2']);
                        $missile->setRessource3Cost($value['CostRessource3']);
                        $missile->setAttractionRadius($value['AttractionRadius']);
                        $missile->setArea(str_replace(',', '.', $value['Area']));
                        $missile->setSpeed(str_replace(',', '.', $value['Speed']));
                        $missile->setTime(str_replace(',', '.', $value['ProdTime']));
                        $missile->setTimeExplosion(str_replace(',', '.', $value['TimeExplosion']));
                        $missile->setSpeedReduc(str_replace(',', '.', $value['SpeedReduc']));
                        $missile->setFreeze(($value['Freeze'] == "True") ? 1 : 0);
                        $missile->setMagnet(($value['Magnet'] == "True") ? 1 : 0);
                        $missile->setDraggable(($value['Draggable'] == "True") ? 1 : 0);
                        $em->persist($missile);
                    }
                }
                $em->flush();
                return $this->redirect($this->generateUrl('csv_missile_level_list_info', array("success" => true)));
                return (null);
            }
            else
               return $this->redirect($this->generateUrl('csv_missile_level_list_info', array("error" => true))); 
        }
        return ($this->render('views/csvMissileLevelListInfoAction.html.php', array(
        'success' => $success, 'error' => $error,
        )));
    }
}