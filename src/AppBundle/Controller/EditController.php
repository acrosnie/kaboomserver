<?php
// src/AppBundle/Controller/ApiController.php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\BuildingPlayer;
use AppBundle\Entity\BuildingList;
use AppBundle\Entity\Player;
use AppBundle\Entity\BuildingLevelList;
use AppBundle\Entity\Img;
use AppBundle\Entity\MissileList;
use AppBundle\Entity\MissileLevelList;
use AppBundle\Entity\MissilePlayer;

class EditController extends Controller
{
	public function addMissilePlayerAction()
	{
		$playerList = $this->getPlayerArray();
		$missileLevelList = $this->getMissileLevelListArray();
		return ($this->render('views/addMissilePlayer.html.php', array("playerList" => $playerList, "missileLevelList" => $missileLevelList)));
	}

	public function editMissilePlayerAction()
	{
		$playerList = $this->getPlayerArray();
		return ($this->render('views/editMissilePlayer.html.php', array("playerList" => $playerList)));
	}

	public function addBuildingPlayerAction()
	{
		return ($this->render('views/addBuildingPlayer.html.php'));
	}

	public function editBuildingPlayerAction()
	{
		return ($this->render('views/editBuildingPlayer.html.php'));
	}

	public function getBuildingPlayerHtmlAction(Request $request, $idPlayer)
	{
		$missilePlayer = $this->getMissilePlayerArray($idPlayer);
		$webDir = $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
		$html = '<div class="row imgSpriteList form-horizontal" style="margin-top: 30px;">';
		$i = 0;
		foreach ($missilePlayer as $key => $value)
		{
			$rand = rand(10000, 20000);
			$head = '<span class="number">'.$i.'</span><div class="idMissilePlayer" style="display:none">'.$value->getId().'</div><span class="name-sprite">'.$value->getMissileLevelList()->getMissileList()->getName().' - Level '.$value->getMissileLevelList()->getMissileLevel().'</span>';
			$selectMissileLevelList = $this->buildSelectMissileLevelListIdInput($value->getMissileLevelList()->getMissileList()->getId());
			$missileLevelListIdInput = '<div class="missileLevelListIdInput form-group" style="position:relative;top:20px;left:40px;"><label>missileLevelList : </label>'.$selectMissileLevelList.'</div>';
			$nbr = '<div class="nbr form-group" style="position:relative;top:20px;left:25px;width:95%;"><label>Nbr: </label><input class="form-control nbr" type="text" name="nbr" id="nbr" value="'.$value->getNbr().'"/></div>';
			$nbrEquiped = '<div class="nbrEquiped form-group" style="position:relative;top:20px;left:25px;width:95%;"><label>Nbr Equiped: </label><input class="form-control nbrEquiped" type="text" name="nbr" id="nbrEquiped" value="'.$value->getNbrEquiped().'"/></div>';
			$buttonEdit = '<div class="edit btn btn-success" style="margin:0 auto;margin-top:40px;display:block;"><span>Edit</span></div>';
			$deleteButton = '<img src="'.$webDir.'/img/delete_icon.png" class="delete" alt="rand="'.$rand.'"" style="width: 64px; height: 64px;left:0px;" data-holder-rendered="true">';
			$html .= '<div class="col-sm-12 dropzoneAuto" id="dropzoneAuto"><div class="demo-content bg-alt">'.$head.$missileLevelListIdInput.$nbr.$nbrEquiped.$buttonEdit.$deleteButton.'</div></div>';
			$i++;
		}
        $html .= "</form></div>";
        return new Response($html);
	}

	public function editMissilePlayerValueAction($idMissilePlayer, $idMissileLevelList, $nbr, $nbrEquiped)
	{
		$respository1 = $this->getDoctrine()
			->getRepository('AppBundle:MissilePlayer');
		$mP = $respository1->find($idMissilePlayer);
		$respository2 = $this->getDoctrine()
			->getRepository('AppBundle:MissileLevelList');
		$mLl = $respository2->find($idMissileLevelList);
		if (isset($mP) && isset($mLl))
		{
			if ($nbr > 0 && $nbr < 100 && $nbrEquiped >= 0 && $nbrEquiped < 100)
			{
				$mP->setMissileLevelList($mLl);
				$mP->setNbr($nbr);
				$mP->setNbrEquiped($nbrEquiped);
				$em = $this->getDoctrine()->getManager();
				$em->flush();
				return new Response("oki");
			}
		}
		return (null);
	}

	public function editDeleteMissilePlayerAction($idMissilePlayer)
	{
		$respository1 = $this->getDoctrine()
			->getRepository('AppBundle:MissilePlayer');
		$mP = $respository1->find($idMissilePlayer);
		if (isset($mP))
		{
	        $em = $this->getDoctrine()->getManager();
	        $em->remove($mP);
	        $em->flush();
			return new Response("delete");
		}
		return (null);
	}

	function buildSelectMissileLevelListIdInput($missileListId)
	{
		$select = '<div class="form-group"><select class="form-control" name="selectMissileLevelList" id="selectMissileLevelList" style="width:90%";>';
		$list = $this->getMissileLevelListArrayWithMissileListId($missileListId);
		foreach ($list as $key => $value)
		{
			$select .= "<option value='".$value->getId()."'>".$value->getMissileList()->getName()." - Level ".$value->getMissileLevel()."</option>";
		}
		$select .= "</select></div>";
		return ($select);
	}

	function getPlayerArray()
	{
		$respository = $this->getDoctrine()
			->getRepository('AppBundle:Player');
		$b = $respository->findAll();
		return ($b);
	}

	function getMissileLevelListArray()
	{
		$respository = $this->getDoctrine()
			->getRepository('AppBundle:MissileLevelList');
		$b = $respository->findAll();
	    return ($b);
	}

	function getMissileLevelListArrayWithMissileListId($missileListId)
	{
		$respository = $this->getDoctrine()
			->getRepository('AppBundle:MissileLevelList');
		$b = $respository->findByMissileList($missileListId);
	    return ($b);
	}

	function getMissilePlayerArray($idPlayer)
	{
		$respository = $this->getDoctrine()
			->getRepository('AppBundle:MissilePlayer');
		$b = $respository->findByPlayer($idPlayer);
	    return ($b);
	}
}
