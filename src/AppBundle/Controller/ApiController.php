<?php
// src/AppBundle/Controller/ApiController.php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\BuildingPlayer;
use AppBundle\Entity\BuildingList;
use AppBundle\Entity\BuildingEffect;
use AppBundle\Entity\player;
use AppBundle\Entity\BuildingLevelList;
use AppBundle\Entity\Img;
use AppBundle\Entity\ImgIcone;
use AppBundle\Entity\ImgMissile;
use AppBundle\Entity\MissileList;
use AppBundle\Entity\MissileLevelList;
use AppBundle\Entity\MissilePlayer;

class ApiController extends Controller
{
    public function homeAction()
    {
        return ($this->render('views/base.html.php'));
    }

    public function getPlayerinfoAction($playerId)
    {
        $p = $this->getPlayerInfoObject($playerId);
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($p,'json');
        return new Response($response);
    }

    public function updatePlayerinfoAction($playerId)
    {
        if ($this->playerExist($playerId))
        {
            $p = $this->getPlayerInfoObject($playerId);
            $p->setDateLastConnection(new \DateTime());
            $p->setOnline(1);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return (new Response("Player Update"));
        }
        return (null);
    }

    public function getSpriteAction(Request $request, $idBuildingLevelList)
    {
        $b = $this->getBuildinglevelListObject($idBuildingLevelList);
        if (!isset($b))
            return (null);
        $imgList = $b->getImg();
        $imgHtml = '<div class="row imgSpriteList form-horizontal" style="margin-top: 30px;"><form id="updateImg" action="" class="form-inline" role="form" method="post" enctype="multipart/form-data">';
        $webDir = $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        if (empty($imgList[0]))
        {
            $imgHtml.= "<p>No Sprite !</p>";
        }
        $i = 0;
        foreach ($imgList as $key => $value)
        {
            $s = explode("_", $value->getName());
            if (!isset($s[0]) || !isset($s[1]))
                continue ;
            $folder = $webDir."/sprite/".$s[0]."_".$s[1]."_".$s[2]."_".$s[3];
            $src = $folder."/".$value->getName();
            $i++;
            $rand = rand(10000, 20000);
            $head = '<span class="number">'.$i.'</span><div class="id-sprite" style="display:none">'.$value->getId().'</div><span class="name-sprite">'.$value->getName().'</span>';
            $headInputNbrImgX = '<div class="nbrInputX form-group" style="position:relative;top:20px;left:40px;"><label>Nbr Img X: </label><input class="form-control nbrImgX" type="text" name="nbrImgX" id="selectNbrX" value="'.$value->getNbrImgX().'"/></div>';
            $headInputNbrImgY = '<div class="nbrInputY form-group" style="position:relative;top:20px;left:80px;"><label>Nbr Img Y: </label><input class="form-control nbrImgY" type="text" name="nbrImgY" id="selectNbrY" value="'.$value->getNbrImgY().'"/></div>';
            $headInputspeed = '<div class="speedInput form-group" style="position:relative;top:20px;left:120px;"><label>Speed : </label><input class="form-control speedImg" type="text" id="inputSpeed" name="speed" value="'.$value->getSpeed().'"/></div>';
            $headInputNbrCase = '<div class="nbrCasee form-group" style="position:relative;top:20px;left:180px;"><label>nbr Case: </label><input class="form-control nbrCase" type="text" id="inputNbrCase" name="nbrCase" value="'.$value->getNbrCase().'"/></div>';
            $buttonSetNbr = '<div class="setNbr btn btn-success" style="margin:0 auto;margin-top:40px;display:block;"><span>Set Value</span></div>';
            $canvas = '';
            $sprite = '<img src="'.$src.'" class="img-thumbnail sprite sim'.$i.'" alt="120x120" style="height: 256px;" data-holder-rendered="true">';
            $editButton = '<div class="fileUpload btn btn-success"><span>Edit Sprite</span><input type="file" id="file-select'.$i.'" class="upload" name="image" accept="image/*"/></div>';
            $deleteButton = '<img src="'.$webDir.'/img/delete_icon.png" class="delete_img" alt="rand="'.$rand.'"" style="width: 64px; height: 64px;" data-holder-rendered="true">';
            $imgHtml .= '<div class="col-sm-12 dropzoneAuto" id="dropzoneAuto"><div class="demo-content bg-alt">'.$head.$sprite.$headInputNbrImgX.$headInputNbrImgY.$headInputspeed.$headInputNbrCase.$buttonSetNbr.$canvas.$editButton.$deleteButton.'</div></div>';
        }
        $imgHtml .= "</form></div>";
        return new Response($imgHtml);
    }

    public function getSpriteMissileAction(Request $request, $idMissileLevelList)
    {
        $b = $this->getMissilelevelListObject($idMissileLevelList);
        if (!isset($b))
            return (null);
        $imgList = $b->getImg();
        $imgHtml = '<div class="row imgSpriteList form-horizontal" style="margin-top: 30px;"><form id="updateImg" action="" class="form-inline" role="form" method="post" enctype="multipart/form-data">';
        $webDir = $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        if (empty($imgList[0]))
        {
            $imgHtml.= "<p>No Sprite !</p>";
        }
        $i = 0;
        foreach ($imgList as $key => $value)
        {
            $s = explode("_", $value->getName());
            if (!isset($s[0]) || !isset($s[1]))
                continue ;
            $folder = $webDir."/sprite/".$s[0]."_".$s[1]."_".$s[2]."_".$s[3];
            $src = $folder."/".$value->getName();
            $i++;
            $rand = rand(10000, 20000);
            $head = '<span class="number">'.$i.'</span><div class="id-sprite" style="display:none">'.$value->getId().'</div><span class="name-sprite">'.$value->getName().'</span>';
            $headInputNbrImgX = '<div class="nbrInputX form-group" style="position:relative;top:20px;left:40px;"><label>Nbr Img X: </label><input class="form-control nbrImgX" type="text" name="nbrImgX" id="selectNbrX" value="'.$value->getNbrImgX().'"/></div>';
            $headInputNbrImgY = '<div class="nbrInputY form-group" style="position:relative;top:20px;left:80px;"><label>Nbr Img Y: </label><input class="form-control nbrImgY" type="text" name="nbrImgY" id="selectNbrY" value="'.$value->getNbrImgY().'"/></div>';
            $headInputspeed = '<div class="speedInput form-group" style="position:relative;top:20px;left:120px;"><label>Speed : </label><input class="form-control speedImg" type="text" id="inputSpeed" name="speed" value="'.$value->getSpeed().'"/></div>';
            $headInputNbrCase = '<div class="nbrCasee form-group" style="position:relative;top:20px;left:180px;"><label>nbr Case: </label><input class="form-control nbrCase" type="text" id="inputNbrCase" name="nbrCase" value="'.$value->getNbrCase().'"/></div>';
            $buttonSetNbr = '<div class="setNbr btn btn-success" style="margin:0 auto;margin-top:40px;display:block;"><span>Set Value</span></div>';
            $canvas = '';
            $sprite = '<img src="'.$src.'" class="img-thumbnail sprite sim'.$i.'" alt="120x120" style="height: 256px;" data-holder-rendered="true">';
            $editButton = '<div class="fileUpload btn btn-success"><span>Edit Sprite</span><input type="file" id="file-select'.$i.'" class="upload" name="image" accept="image/*"/></div>';
            $deleteButton = '<img src="'.$webDir.'/img/delete_icon.png" class="delete_img" alt="rand="'.$rand.'"" style="width: 64px; height: 64px;" data-holder-rendered="true">';
            $imgHtml .= '<div class="col-sm-12 dropzoneAuto" id="dropzoneAuto"><div class="demo-content bg-alt">'.$head.$sprite.$headInputNbrImgX.$headInputNbrImgY.$headInputspeed.$headInputNbrCase.$buttonSetNbr.$canvas.$editButton.$deleteButton.'</div></div>';
        }
        $imgHtml .= "</form></div>";
        return new Response($imgHtml);
    }

    public function getSpriteMissileIconeAction(Request $request, $idMissileLevelList)
    {
        $b = $this->getMissilelevelListObject($idMissileLevelList);
        if (!isset($b))
            return (null);
        $img = $b->getImgIcone();
        $img = $img[0];
        $imgHtml = '<div class="row imgSpriteList form-horizontal" style="margin-top: 30px;"><form id="updateImg" action="" class="form-inline" role="form" method="post" enctype="multipart/form-data">';
        $webDir = $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        if (empty($img))
        {
            $imgHtml .= "<p>No Sprite !</p>";
        }
        $i = 0;
        if (isset ($img) && $img != "")
        {
            $s = explode("_", $img->getName());
            if (!isset($s[0]) || !isset($s[1]))
                continue ;
            $folder = $webDir."/sprite/".$s[0]."_".$s[1]."_".$s[2]."_".$s[3];
            $src = $folder."/".$img->getName();
            $i++;
            $rand = rand(10000, 20000);
            $head = '<span class="number">'.$i.'</span><div class="id-sprite" style="display:none">'.$img->getId().'</div><span class="name-sprite">'.$img->getName().'</span>';
            $sprite = '<img src="'.$src.'" class="img-thumbnail sprite sim'.$i.'" alt="120x120" style="height: 256px;" data-holder-rendered="true">';
            $deleteButton = '<img src="'.$webDir.'/img/delete_icon.png" class="delete_img" alt="rand="'.$rand.'"" style="width: 64px; height: 64px;" data-holder-rendered="true">';
            $imgHtml .= '<div class="col-sm-12 dropzoneAuto" id="dropzoneAuto"><div class="demo-content bg-alt">'.$head.$sprite.$deleteButton.'</div></div>';
        }
        $imgHtml .= "</form></div>";
        return new Response($imgHtml);
    }

    public function getBuildinglevelListObject($buildingLevelListId)
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:BuildingLevelList');
        $b = $respository->find($buildingLevelListId);
        return ($b);
    }

    public function getMissilelevelListObject($missileLevelListId)
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:MissileLevelList');
        $b = $respository->find($missileLevelListId);
        return ($b);
    }

    public function getPlayerInfoObject($playerId)
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:Player');
        $b = $respository->find($playerId);
        return ($b);
    }

    public function getBuildingListAction()
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:BuildingList');
        $b = $respository->findAll();
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($b,'json');
        return new Response($response);
    }

    public function getMissileListAction()
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:MissileList');
        $b = $respository->findAll();
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($b,'json');
        return new Response($response);
    }

    public function getBuildingPlayerListAction($playerId)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT a FROM AppBundle:BuildingPlayer a WHERE a.player = :id');
        $query->setParameter('id', $playerId);
        $b = $query->getResult();
        $now = date('m/d/Y h:i:s a', time());
        foreach ($b as $key => $value)
        {
            $last = $value->getlastUpdate()->format('m/d/Y h:i:s a');
            $diff = strtotime($now) - strtotime($last);
            $value->setDifferenceInSeconds($diff);
        }
        $em->flush();
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($b,'json');
        return new Response($response);
    }

    public function getMissilePlayerListAction($playerId)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT a FROM AppBundle:MissilePlayer a WHERE a.player = :id');
        $query->setParameter('id', $playerId);
        $b = $query->getResult();
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($b,'json');
        return new Response($response);
    }

    public function getBuildinglevelListAction()
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:BuildingLevelList');
        $b = $respository->findAll();
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($b,'json');
        return new Response($response);
    }

    public function getBuildingEffectListAction()
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:BuildingEffect');
        $b = $respository->findAll();
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($b,'json');
        return new Response($response);
    }

    public function getMissilelevelListAction()
    {
        $respository = $this->getDoctrine()
            ->getRepository('AppBundle:MissileLevelList');
        $b = $respository->findAll();
        $serializer = $this->get('jms_serializer');
        $response = $serializer->serialize($b,'json');
        return new Response($response);
    }

    public function addBuildingPlayerAction($playerId, $buildingLevelListId, $x, $y, $z)
    {
        if ($this->playerExist($playerId) && $this->buildingLevelListExist($buildingLevelListId))
        {
            $player = $this->getDoctrine()
                    ->getRepository('AppBundle:Player')
                    ->find($playerId);
            $buildingLevelList = $this->getDoctrine()
                    ->getRepository('AppBundle:BuildingLevelList')
                    ->find($buildingLevelListId);
            $building = new BuildingPlayer();
            $building->setPlayer($player);
            $building->setBuildingLevelList($buildingLevelList);
            $building->setX($x);
            $building->setY($y);
            $building->setZ($z);
            $building->setDateCreate(new \DateTime());
            $building->setNbrRessource1(0);
            $building->setNbrRessource2(0);
            $building->setNbrRessource3(0);
            $building->setLastUpdate(new \DateTime());
            $building->setDifferenceInSeconds(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($building);
            $em->flush();
            return new Response($building->getId());
        }
    }

    public function getMissilePlayer($playerId, $missileLevelListId)
    {
        $em = $this->getDoctrine()->getManager();
        $missilePlayer = $em->getRepository('AppBundle:MissilePlayer')->findByMissileLevelList($missileLevelListId);
        if (Count($missilePlayer) > 0)
            return ($missilePlayer[0]);
        return (null);
    }

    public function addMissilePlayerAction($playerId, $missileLevelListId)
    {
        if ($this->playerExist($playerId) && $this->missileLevelListExist($missileLevelListId))
        {
            $em = $this->getDoctrine()->getManager();
            $player = $this->getDoctrine()
                    ->getRepository('AppBundle:Player')
                    ->find($playerId);
            $missileLevelList = $this->getDoctrine()
                    ->getRepository('AppBundle:MissileLevelList')
                    ->find($missileLevelListId);
            $missilePlayer = $this->getMissilePlayer($playerId, $missileLevelListId);
            if ($missilePlayer == null)
            {
                $missile = new MissilePlayer();
                $missile->setPlayer($player);
                $missile->setMissileLevelList($missileLevelList);
                $missile->setDateCreation(new \DateTime());
                $missile->setDateUpdate(new \DateTime());
                $missile->setNbr(1);
                $missile->setNbrEquiped(0);
                $em->persist($missile);
            }
            else
            {
                $missilePlayer->setNbr($missilePlayer->getNbr() + 1);
                $missilePlayer->setDateUpdate(new \DateTime());
                $em->persist($missilePlayer);
            }
            $em->flush();
            if ($missilePlayer == null)
                return new Response($missile->getId());
            return new Response($missilePlayer->getId());
        }
    }

    public function updateBuildingPlayerAction($playerId, $buildingLevelListId, $buildingPlayerId, $x, $y, $z, $r1, $r2, $r3)
    {
        $oldBuildingPlayer = $this->getDoctrine()
            ->getRepository('AppBundle:BuildingPlayer')
            ->find($buildingPlayerId);
        $newBuildingLevelList = $this->getDoctrine()
            ->getRepository('AppBundle:buildingLevelList')
            ->find($buildingLevelListId);
        if ($this->verifInfoUpdateBuildingPlayer($playerId, $oldBuildingPlayer, $newBuildingLevelList))
        {
            $em = $this->getDoctrine()->getManager();
            $oldBuildingPlayer->setBuildingLevelList($newBuildingLevelList);
            $oldBuildingPlayer->setX($x);
            $oldBuildingPlayer->setY($y);
            $oldBuildingPlayer->setZ($z);
            $lastDate = $oldBuildingPlayer->getlastUpdate()->getTimestamp();
            $newDate = new \DateTime();
            $oldBuildingPlayer->setLastUpdate(new \DateTime());
            $oldBuildingPlayer->setDifferenceInSeconds($newDate->getTimestamp() - $lastDate);
            $r1 = ($r1 > $newBuildingLevelList->getCapacityRessource1() && $newBuildingLevelList->getStockRessource1() == 0) ? $newBuildingLevelList->getCapacityRessource1() : $r1;
            $r2 = ($r2 > $newBuildingLevelList->getCapacityRessource2() && $newBuildingLevelList->getStockRessource2() == 0) ? $newBuildingLevelList->getCapacityRessource2() : $r2;
            $r3 = ($r3 > $newBuildingLevelList->getCapacityRessource3() && $newBuildingLevelList->getStockRessource3() == 0) ? $newBuildingLevelList->getCapacityRessource3() : $r3;
            $r1 = ($r1 > $newBuildingLevelList->getStockRessource1() && $newBuildingLevelList->getProdRateRessource1 == 0) ? $newBuildingLevelList->getStockRessource1() : $r1;
            $r2 = ($r2 > $newBuildingLevelList->getStockRessource2() && $newBuildingLevelList->getProdRateRessource2 == 0) ? $newBuildingLevelList->getStockRessource2() : $r2;
            $r3 = ($r3 > $newBuildingLevelList->getStockRessource3() && $newBuildingLevelList->getProdRateRessource3 == 0) ? $newBuildingLevelList->getStockRessource3() : $r3;
            $oldBuildingPlayer->setNbrRessource1($r1);
            $oldBuildingPlayer->setNbrRessource2($r2);
            $oldBuildingPlayer->setNbrRessource3($r3);
            $em->flush();
            return new Response("hello");
        }
    }

    public function verifInfoUpdateBuildingPlayer($playerId, $oldBuildingPlayer, $newBuildingLevelList)
    {
        if (!$this->playerExist($playerId) || !isset($oldBuildingPlayer) || !isset($newBuildingLevelList)) // les donnes recu sont elles fiables ?
            return (false);
        if ($oldBuildingPlayer->getPlayer()->getId() != $playerId) // le batiment appartient il bien a ce joueur ?
            return (false);
        if ($oldBuildingPlayer->getBuildingLevelList()->getBuildingList()->getId() != $newBuildingLevelList->getBuildingList()->getId()) // s'agit il du meme batiment ?
            return (false);
        return (true);
    }

    public function playerExist($playerId)
    {
        $player = $this->getDoctrine()
                ->getRepository('AppBundle:Player')
                ->find($playerId);
        if ($player == null)
            return (false);
        return (true);
    }

    public function buildingExist($buildingId)
    {
        $building = $this->getDoctrine()
                ->getRepository('AppBundle:BuildingList')
                ->find($buildingId);
        if ($building == null)
            return (false);
        return (true);
    }

    public function buildingLevelListExist($buildingId)
    {
        $building = $this->getDoctrine()
                ->getRepository('AppBundle:BuildingLevelList')
                ->find($buildingId);
        if ($building == null)
            return (false);
        return (true);
    }

    public function missileLevelListExist($missileId)
    {
        $missile = $this->getDoctrine()
                ->getRepository('AppBundle:MissileLevelList')
                ->find($missileId);
        if ($missile == null)
            return (false);
        return (true);
    }

    public function buildingPlayerExist($buildingPlayerId)
    {
        $building = $this->getDoctrine()
                ->getRepository('AppBundle:BuildingPlayer')
                ->find($buildingPlayerId);
        if ($building == null)
            return (false);
        return (true);
    }
}
