<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BuildingPlayer
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class BuildingPlayer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player")
    * @ORM\JoinColumn(nullable=false)
    */
    private $player;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BuildingLevelList")
    * @ORM\JoinColumn(nullable=false)
    */
    private $buildingLevelList;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float")
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float")
     */
    private $y;

    /**
     * @var float
     *
     * @ORM\Column(name="z", type="float")
     */
    private $z;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreate", type="datetime")
     */
    private $dateCreate;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrRessource1", type="integer")
     */
    private $nbrRessource1;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrRessource2", type="integer")
     */
    private $nbrRessource2;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrRessource3", type="integer")
     */
    private $nbrRessource3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastUpdate", type="datetime")
     */
    private $lastUpdate;

    /**
     * @var integer
     *
     * @ORM\Column(name="differenceInSeconds", type="integer")
     */
    private $differenceInSeconds;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setPlayer(Player $player = null)
    {
        $this->player = $player;
    }

    public function getPlayer()
    {
        return $this->player;
    }

    public function setBuildingLevelList(BuildingLevelList $buildingLevelList = null)
    {
        $this->buildingLevelList = $buildingLevelList;

        return $this;
    }

    public function getBuildingLevelList()
    {
        return $this->buildingLevelList;
    }

    /**
     * Set x
     *
     * @param float $x
     *
     * @return BuildingPlayer
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return float
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param float $y
     *
     * @return BuildingPlayer
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return float
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set z
     *
     * @param float $z
     *
     * @return BuildingPlayer
     */
    public function setZ($z)
    {
        $this->z = $z;

        return $this;
    }

    /**
     * Get z
     *
     * @return float
     */
    public function getZ()
    {
        return $this->z;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return BuildingPlayer
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set nbrRessource1
     *
     * @param integer $nbrRessource1
     *
     * @return BuildingPlayer
     */
    public function setNbrRessource1($nbrRessource1)
    {
        $this->nbrRessource1 = $nbrRessource1;

        return $this;
    }

    /**
     * Get nbrRessource1
     *
     * @return integer
     */
    public function getNbrRessource1()
    {
        return $this->nbrRessource1;
    }

    /**
     * Set nbrRessource2
     *
     * @param integer $nbrRessource2
     *
     * @return BuildingPlayer
     */
    public function setNbrRessource2($nbrRessource2)
    {
        $this->nbrRessource2 = $nbrRessource2;

        return $this;
    }

    /**
     * Get nbrRessource2
     *
     * @return integer
     */
    public function getNbrRessource2()
    {
        return $this->nbrRessource2;
    }

    /**
     * Set nbrRessource3
     *
     * @param integer $nbrRessource3
     *
     * @return BuildingPlayer
     */
    public function setNbrRessource3($nbrRessource3)
    {
        $this->nbrRessource3 = $nbrRessource3;

        return $this;
    }

    /**
     * Get nbrRessource3
     *
     * @return integer
     */
    public function getNbrRessource3()
    {
        return $this->nbrRessource3;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return BuildingPlayer
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set differenceInSeconds
     *
     * @param integer $differenceInSeconds
     *
     * @return BuildingPlayer
     */
    public function setDifferenceInSeconds($differenceInSeconds)
    {
        $this->differenceInSeconds = $differenceInSeconds;

        return $this;
    }

    /**
     * Get differenceInSeconds
     *
     * @return integer
     */
    public function getDifferenceInSeconds()
    {
        return $this->differenceInSeconds;
    }
}

