<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MissilePlayer
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MissilePlayer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MissileLevelList")
    * @ORM\JoinColumn(nullable=false)
    */
    private $missileLevelList;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player")
    * @ORM\JoinColumn(nullable=false)
    */
    private $player;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbr", type="integer")
     */
    private $nbr;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrEquiped", type="integer")
     */
    private $nbrEquiped;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="date", nullable=true)
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateUpdate", type="date")
     */
    private $dateUpdate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setMissileLevelList(MissileLevelList $missileLevelList = null)
    {
        $this->missileLevelList = $missileLevelList;

        return $this;
    }

    public function getMissileLevelList()
    {
        return $this->missileLevelList;
    }

    public function setPlayer(Player $player = null)
    {
        $this->player = $player;
    }

    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set nbr
     *
     * @param integer $nbr
     *
     * @return MissilePlayer
     */
    public function setNbr($nbr)
    {
        $this->nbr = $nbr;

        return $this;
    }

    /**
     * Get nbr
     *
     * @return integer
     */
    public function getNbr()
    {
        return $this->nbr;
    }

    /**
     * Set nbrEquiped
     *
     * @param integer $nbrEquiped
     *
     * @return MissilePlayer
     */
    public function setNbrEquiped($nbrEquiped)
    {
        $this->nbrEquiped = $nbrEquiped;

        return $this;
    }

    /**
     * Get nbrEquiped
     *
     * @return integer
     */
    public function getNbrEquiped()
    {
        return $this->nbrEquiped;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return MissilePlayer
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return MissilePlayer
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }
}

