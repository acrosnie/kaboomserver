<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImgMissile
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ImgMissile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="randomId", type="integer")
     */
    private $randomId;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrImgX", type="integer")
     */
    private $nbrImgX;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrImgY", type="integer")
     */
    private $nbrImgY;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrCase", type="integer")
     */
    private $nbrCase;

    /**
     * @var integer
     *
     * @ORM\Column(name="speed", type="integer")
     */
    private $speed;


    /**
     * @var MissileLevelList $missileLevelList
     *
     * @ORM\ManyToOne(targetEntity="MissileLevelList", inversedBy="img", cascade={"persist", "merge"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $missileLevelList;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set randomId
     *
     * @param integer $randomId
     *
     * @return Img
     */
    public function setRandomId($randomId)
    {
        $this->randomId = $randomId;

        return $this;
    }

    /**
     * Get randomId
     *
     * @return integer
     */
    public function getRandomId()
    {
        return $this->randomId;
    }

    /**
     * Set name
     *
     * @param integer $name
     *
     * @return Img
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return integer
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nbrImgX
     *
     * @param integer $nbrImgX
     *
     * @return Img
     */
    public function setNbrImgX($nbrImgX)
    {
        $this->nbrImgX = $nbrImgX;

        return $this;
    }

    /**
     * Get nbrImgX
     *
     * @return integer
     */
    public function getNbrImgX()
    {
        return $this->nbrImgX;
    }

    /**
     * Set nbrImgY
     *
     * @param integer $nbrImgY
     *
     * @return Img
     */
    public function setNbrImgY($nbrImgY)
    {
        $this->nbrImgY = $nbrImgY;

        return $this;
    }

    /**
     * Get nbrImgY
     *
     * @return integer
     */
    public function getNbrImgY()
    {
        return $this->nbrImgY;
    }

    /**
     * Set nbrCase
     *
     * @param integer $nbrCase
     *
     * @return Img
     */
    public function setNbrCase($nbrCase)
    {
        $this->nbrCase = $nbrCase;

        return $this;
    }

    /**
     * Get nbrCase
     *
     * @return integer
     */
    public function getNbrCase()
    {
        return $this->nbrCase;
    }

    /**
     * Set speed
     *
     * @param integer $speed
     *
     * @return Img
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return integer
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param MissileLevelList $missileLevelList
     */

    public function setMissileLevelList(MissileLevelList $missileLevelList)
    {
        $this->missileLevelList = $missileLevelList;

        return $this;
    }

    /**
     * @return MissileLevelList $missileLevelList
     */
    public function getMissileLevelList()
    {
        return $this->missileLevelList;
    }
}

