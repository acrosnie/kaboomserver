<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DoctrineCommonCollectionsArrayCollection;
/**
 * BuildingLevelList
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class BuildingLevelList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BuildingList", cascade={"persist", "remove"})
    * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
    */
    private $buildingList;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\BuildingEffect", cascade={"persist", "remove", "merge"})
    * @ORM\JoinColumn(nullable=true)
    */

    private $buildingEffect;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level;

    /**
     * @var float
     *
     * @ORM\Column(name="health", type="float")
     */
    private $health;

    /**
     * @var integer
     *
     * @ORM\Column(name="CostRessource1Build", type="integer")
     */
    private $CostRessource1Build;

    /**
     * @var integer
     *
     * @ORM\Column(name="CostRessource2Build", type="integer")
     */
    private $CostRessource2Build;

    /**
     * @var integer
     *
     * @ORM\Column(name="CostRessource3Build", type="integer")
     */
    private $CostRessource3Build;

    /**
     * @var integer
     *
     * @ORM\Column(name="CostRessource1Upgrade", type="integer")
     */
    private $CostRessource1Upgrade;

    /**
     * @var integer
     *
     * @ORM\Column(name="CostRessource2Upgrade", type="integer")
     */
    private $CostRessource2Upgrade;

    /**
     * @var integer
     *
     * @ORM\Column(name="CostRessource3Upgrade", type="integer")
     */
    private $CostRessource3Upgrade;

    /**
     * @var integer
     *
     * @ORM\Column(name="time", type="integer")
     */
    private $time;

    /**
     * @var integer
     *
     * @ORM\Column(name="capacityRessource1", type="integer")
     */
    private $capacityRessource1;

    /**
     * @var integer
     *
     * @ORM\Column(name="capacityRessource2", type="integer")
     */
    private $capacityRessource2;

    /**
     * @var integer
     *
     * @ORM\Column(name="capacityRessource3", type="integer")
     */
    private $capacityRessource3;

    /**
     * @var integer
     *
     * @ORM\Column(name="stockRessource1", type="integer")
     */
    private $stockRessource1;

    /**
     * @var integer
     *
     * @ORM\Column(name="stockRessource2", type="integer")
     */
    private $stockRessource2;

    /**
     * @var integer
     *
     * @ORM\Column(name="stockRessource3", type="integer")
     */
    private $stockRessource3;

    /**
     * @var float
     *
     * @ORM\Column(name="prodRateRessource1", type="float")
     */
    private $prodRateRessource1;

    /**
     * @var float
     *
     * @ORM\Column(name="prodRateRessource2", type="float")
     */
    private $prodRateRessource2;

    /**
     * @var float
     *
     * @ORM\Column(name="prodRateRessource3", type="float")
     */
    private $prodRateRessource3;

    /**
     * @var integer
     *
     * @ORM\Column(name="buyable", type="integer")
     */
    private $buyable;

    /**
     * @var integer
     *
     * @ORM\Column(name="preConstructed", type="integer")
     */
    private $preConstructed;

    /**
     * @var ArrayCollection $img
     *
     * @ORM\OneToMany(targetEntity="Img", mappedBy="buildingLevelList", cascade={"persist", "remove", "merge"})
     */
    private $img;

    /**
     * Get id
     *
     * @return integer
     */

    public function getId()
    {
        return $this->id;
    }

    public function setBuildingList($buildingList)
    {
        $this->buildingList = $buildingList;

        return $this;
    }

    public function getBuildingList()
    {
        return $this->buildingList;
    }

    public function setBuildingEffect($buildingEffect)
    {
        $this->buildingEffect = $buildingEffect;

        return $this;
    }

    public function getBuildingEffect()
    {
        return $this->buildingEffect;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return BuildingLevelList
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set health
     *
     * @param float $health
     *
     * @return BuildingLevelList
     */
    public function setHealth($health)
    {
        $this->health = $health;

        return $this;
    }

    /**
     * Get health
     *
     * @return float
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * Set CostRessource1Build
     *
     * @param integer $CostRessource1Build
     *
     * @return BuildingLevelList
     */
    public function setCostRessource1Build($CostRessource1Build)
    {
        $this->CostRessource1Build = $CostRessource1Build;

        return $this;
    }

    /**
     * Get CostRessource1Build
     *
     * @return integer
     */
    public function getCostRessource1Build()
    {
        return $this->CostRessource1Build;
    }

    /**
     * Set CostRessource2Build
     *
     * @param integer $CostRessource2Build
     *
     * @return BuildingLevelList
     */
    public function setCostRessource2Build($CostRessource2Build)
    {
        $this->CostRessource2Build = $CostRessource2Build;

        return $this;
    }

    /**
     * Get CostRessource2Build
     *
     * @return integer
     */
    public function getCostRessource2Build()
    {
        return $this->CostRessource2Build;
    }

    /**
     * Set CostRessource3Build
     *
     * @param integer $CostRessource3Build
     *
     * @return BuildingLevelList
     */
    public function setCostRessource3Build($CostRessource3Build)
    {
        $this->CostRessource3Build = $CostRessource3Build;

        return $this;
    }

    /**
     * Get CostRessource3Build
     *
     * @return integer
     */
    public function getCostRessource3Build()
    {
        return $this->CostRessource3Build;
    }

    /**
     * Set CostRessource1Upgrade
     *
     * @param integer $CostRessource1Upgrade
     *
     * @return BuildingLevelList
     */
    public function setCostRessource1Upgrade($CostRessource1Upgrade)
    {
        $this->CostRessource1Upgrade = $CostRessource1Upgrade;

        return $this;
    }

    /**
     * Get CostRessource1Upgrade
     *
     * @return integer
     */
    public function getCostRessource1Upgrade()
    {
        return $this->CostRessource1Upgrade;
    }

    /**
     * Set CostRessource2Upgrade
     *
     * @param integer $CostRessource2Upgrade
     *
     * @return BuildingLevelList
     */
    public function setCostRessource2Upgrade($CostRessource2Upgrade)
    {
        $this->CostRessource2Upgrade = $CostRessource2Upgrade;

        return $this;
    }

    /**
     * Get CostRessource2Upgrade
     *
     * @return integer
     */
    public function getCostRessource2Upgrade()
    {
        return $this->CostRessource2Upgrade;
    }

    /**
     * Set CostRessource3Upgrade
     *
     * @param integer $CostRessource3Upgrade
     *
     * @return BuildingLevelList
     */
    public function setCostRessource3Upgrade($CostRessource3Upgrade)
    {
        $this->CostRessource3Upgrade = $CostRessource3Upgrade;

        return $this;
    }

    /**
     * Get CostRessource3Upgrade
     *
     * @return integer
     */
    public function getCostRessource3Upgrade()
    {
        return $this->CostRessource3Upgrade;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return BuildingLevelList
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return integer
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set capacityRessource1
     *
     * @param integer $capacityRessource1
     *
     * @return BuildingLevelList
     */
    public function setCapacityRessource1($capacityRessource1)
    {
        $this->capacityRessource1 = $capacityRessource1;

        return $this;
    }

    /**
     * Get capacityRessource1
     *
     * @return integer
     */
    public function getCapacityRessource1()
    {
        return $this->capacityRessource1;
    }

    /**
     * Set capacityRessource2
     *
     * @param integer $capacityRessource2
     *
     * @return BuildingLevelList
     */
    public function setCapacityRessource2($capacityRessource2)
    {
        $this->capacityRessource2 = $capacityRessource2;

        return $this;
    }

    /**
     * Get capacityRessource2
     *
     * @return integer
     */
    public function getCapacityRessource2()
    {
        return $this->capacityRessource2;
    }

    /**
     * Set capacityRessource3
     *
     * @param integer $capacityRessource3
     *
     * @return BuildingLevelList
     */
    public function setCapacityRessource3($capacityRessource3)
    {
        $this->capacityRessource3 = $capacityRessource3;

        return $this;
    }

    /**
     * Get capacityRessource3
     *
     * @return integer
     */
    public function getCapacityRessource3()
    {
        return $this->capacityRessource3;
    }

    /**
     * Set stockRessource1
     *
     * @param integer $stockRessource1
     *
     * @return BuildingLevelList
     */
    public function setStockRessource1($stockRessource1)
    {
        $this->stockRessource1 = $stockRessource1;

        return $this;
    }

    /**
     * Get stockRessource1
     *
     * @return integer
     */
    public function getStockRessource1()
    {
        return $this->stockRessource1;
    }

    /**
     * Set stockRessource2
     *
     * @param integer $stockRessource2
     *
     * @return BuildingLevelList
     */
    public function setStockRessource2($stockRessource2)
    {
        $this->stockRessource2 = $stockRessource2;

        return $this;
    }

    /**
     * Get stockRessource2
     *
     * @return integer
     */
    public function getStockRessource2()
    {
        return $this->stockRessource2;
    }

    /**
     * Set stockRessource3
     *
     * @param integer $stockRessource3
     *
     * @return BuildingLevelList
     */
    public function setStockRessource3($stockRessource3)
    {
        $this->stockRessource3 = $stockRessource3;

        return $this;
    }

    /**
     * Get stockRessource3
     *
     * @return integer
     */
    public function getStockRessource3()
    {
        return $this->stockRessource3;
    }

    /**
     * Set prodRateRessource1
     *
     * @param float $prodRateRessource1
     *
     * @return BuildingLevelList
     */
    public function setProdRateRessource1($prodRateRessource1)
    {
        $this->prodRateRessource1 = $prodRateRessource1;

        return $this;
    }

    /**
     * Get prodRateRessource1
     *
     * @return float
     */
    public function getProdRateRessource1()
    {
        return $this->prodRateRessource1;
    }

    /**
     * Set prodRateRessource2
     *
     * @param float $prodRateRessource2
     *
     * @return BuildingLevelList
     */
    public function setProdRateRessource2($prodRateRessource2)
    {
        $this->prodRateRessource2 = $prodRateRessource2;

        return $this;
    }

    /**
     * Get prodRateRessource2
     *
     * @return float
     */
    public function getProdRateRessource2()
    {
        return $this->prodRateRessource2;
    }

    /**
     * Set prodRateRessource3
     *
     * @param float $prodRateRessource3
     *
     * @return BuildingLevelList
     */
    public function setProdRateRessource3($prodRateRessource3)
    {
        $this->prodRateRessource3 = $prodRateRessource3;

        return $this;
    }

    /**
     * Get prodRateRessource3
     *
     * @return float
     */
    public function getProdRateRessource3()
    {
        return $this->prodRateRessource3;
    }

    /**
     * Get img
     *
     * @return text
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set buyable
     *
     * @param int $buyable
     *
     * @return BuildingLevelList
     */
    public function setBuyable($buyable)
    {
        $this->buyable = $buyable;

        return $this;
    }

    /**
     * Get buyable
     *
     * @return int
     */
    public function getBuyable()
    {
        return $this->buyable;
    }

    /**
     * Set preConstructed
     *
     * @param int $preConstructed
     *
     * @return BuildingLevelList
     */
    public function setPreConstructed($preConstructed)
    {
        $this->preConstructed = $preConstructed;

        return $this;
    }

    /**
     * Get preConstructed
     *
     * @return int
     */
    public function getPreConstructed()
    {
        return $this->preConstructed;
    }
}

