<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MissileList
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MissileList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="att", type="integer")
     */
    private $att;

    /**
     * @var integer
     *
     * @ORM\Column(name="def", type="integer")
     */
    private $def;

    /**
     * @var integer
     *
     * @ORM\Column(name="lvlMissileFact", type="integer")
     */
    private $lvlMissileFact;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MissileList
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return MissileList
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set att
     *
     * @param integer $att
     *
     * @return MissileList
     */
    public function setAtt($att)
    {
        $this->att = $att;

        return $this;
    }

    /**
     * Get att
     *
     * @return integer
     */
    public function getAtt()
    {
        return $this->att;
    }

    /**
     * Set def
     *
     * @param integer $def
     *
     * @return MissileList
     */
    public function setDef($def)
    {
        $this->def = $def;

        return $this;
    }

    /**
     * Get def
     *
     * @return integer
     */
    public function getDef()
    {
        return $this->def;
    }

    /**
     * Set lvlMissileFact
     *
     * @param integer $lvlMissileFact
     *
     * @return MissileList
     */
    public function setLvlMissileFact($lvlMissileFact)
    {
        $this->lvlMissileFact = $lvlMissileFact;

        return $this;
    }

    /**
     * Get lvlMissileFact
     *
     * @return integer
     */
    public function getLvlMissileFact()
    {
        return $this->lvlMissileFact;
    }
}

