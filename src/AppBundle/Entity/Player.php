<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Player
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pseudo", type="string", length=100)
     */
    private $pseudo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateLastConnection", type="datetime")
     */
    private $dateLastConnection;

    /**
     * @var integer
     *
     * @ORM\Column(name="online", type="integer")
     */
    private $online;

    /**
     * @var integer
     *
     * @ORM\Column(name="idTitle", type="integer")
     */
    private $idTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="levelGeneral", type="integer")
     */
    private $levelGeneral;

    /**
     * @var integer
     *
     * @ORM\Column(name="ranking", type="integer")
     */
    private $ranking;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pseudo
     *
     * @param string $pseudo
     *
     * @return Player
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * Get pseudo
     *
     * @return string
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Player
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateLastConnection
     *
     * @param \DateTime $dateLastConnection
     *
     * @return Player
     */
    public function setDateLastConnection($dateLastConnection)
    {
        $this->dateLastConnection = $dateLastConnection;

        return $this;
    }

    /**
     * Get dateLastConnection
     *
     * @return \DateTime
     */
    public function getDateLastConnection()
    {
        return $this->dateLastConnection;
    }

    /**
     * Set online
     *
     * @param integer $online
     *
     * @return Player
     */
    public function setOnline($online)
    {
        $this->online = $online;

        return $this;
    }

    /**
     * Get online
     *
     * @return integer
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * Set idTitle
     *
     * @param integer $idTitle
     *
     * @return Player
     */
    public function setIdTitle($idTitle)
    {
        $this->idTitle = $idTitle;

        return $this;
    }

    /**
     * Get idTitle
     *
     * @return integer
     */
    public function getIdTitle()
    {
        return $this->idTitle;
    }

    /**
     * Set levelGeneral
     *
     * @param integer $levelGeneral
     *
     * @return Player
     */
    public function setLevelGeneral($levelGeneral)
    {
        $this->levelGeneral = $levelGeneral;

        return $this;
    }

    /**
     * Get levelGeneral
     *
     * @return integer
     */
    public function getLevelGeneral()
    {
        return $this->levelGeneral;
    }

    /**
     * Set ranking
     *
     * @param integer $ranking
     *
     * @return Player
     */
    public function setRanking($ranking)
    {
        $this->ranking = $ranking;

        return $this;
    }

    /**
     * Get ranking
     *
     * @return integer
     */
    public function getRanking()
    {
        return $this->ranking;
    }
}

