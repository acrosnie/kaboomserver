<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DoctrineCommonCollectionsArrayCollection;

/**
 * MissileLevelList
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MissileLevelList
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MissileList", cascade={"persist", "remove"})
    * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
    */
    private $missileList;

    /**
     * @var integer
     *
     * @ORM\Column(name="damageMissile", type="integer")
     */
    private $damageMissile;

    /**
     * @var integer
     *
     * @ORM\Column(name="damageBuilding", type="integer")
     */
    private $damageBuilding;

    /**
     * @var integer
     *
     * @ORM\Column(name="health", type="integer")
     */
    private $health;

    /**
     * @var integer
     *
     * @ORM\Column(name="missileLevel", type="integer")
     */
    private $missileLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="labUnlockLevel", type="integer")
     */
    private $labUnlockLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="ressource1Cost", type="integer")
     */
    private $ressource1Cost;

    /**
     * @var integer
     *
     * @ORM\Column(name="ressource2Cost", type="integer")
     */
    private $ressource2Cost;

    /**
     * @var integer
     *
     * @ORM\Column(name="ressource3Cost", type="integer")
     */
    private $ressource3Cost;

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="float")
     */
    private $area;

    /**
     * @var float
     *
     * @ORM\Column(name="speed", type="float")
     */
    private $speed;

    /**
     * @var float
     *
     * @ORM\Column(name="time", type="float")
     */
    private $time;

    /**
     * @var float
     *
     * @ORM\Column(name="timeExplosion", type="float")
     */
    private $timeExplosion;

    /**
     * @var float
     *
     * @ORM\Column(name="coolDown", type="float")
     */
    private $coolDown;

    /**
     * @var integer
     *
     * @ORM\Column(name="freeze", type="integer")
     */
    private $freeze;

    /**
     * @var integer
     *
     * @ORM\Column(name="magnet", type="integer")
     */
    private $magnet;

    /**
     * @var integer
     *
     * @ORM\Column(name="attractionRadius", type="integer")
     */
    private $attractionRadius;

    /**
     * @var integer
     *
     * @ORM\Column(name="draggable", type="integer")
     */
    private $draggable;

    /**
     * @var float
     *
     * @ORM\Column(name="speedReduc", type="float")
     */
    private $speedReduc;

    /**
     * @var ArrayCollection $img
     *
     * @ORM\OneToMany(targetEntity="ImgMissile", mappedBy="missileLevelList", cascade={"persist", "remove", "merge"})
     */
    private $img;

    /**
     * @var ArrayCollection $imgIcone
     *
     * @ORM\OneToMany(targetEntity="ImgIcone", mappedBy="missileLevelList", cascade={"persist", "remove", "merge"})
     */
    private $imgIcone;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setMissileList($missileList)
    {
        $this->missileList = $missileList;
        return $this;
    }

    public function getMissileList()
    {
        return $this->missileList;
    }

    /**
     * Set damageMissile
     *
     * @param integer $damageMissile
     *
     * @return MissileLevelList
     */
    public function setDamageMissile($damageMissile)
    {
        $this->damageMissile = $damageMissile;

        return $this;
    }

    /**
     * Get damageMissile
     *
     * @return integer
     */
    public function getDamageMissile()
    {
        return $this->damageMissile;
    }

    /**
     * Set damageBuilding
     *
     * @param integer $damageBuilding
     *
     * @return MissileLevelList
     */
    public function setDamageBuilding($damageBuilding)
    {
        $this->damageBuilding = $damageBuilding;

        return $this;
    }

    /**
     * Get damageBuilding
     *
     * @return integer
     */
    public function getDamageBuilding()
    {
        return $this->damageBuilding;
    }

    /**
     * Set health
     *
     * @param integer $health
     *
     * @return MissileLevelList
     */
    public function setHealth($health)
    {
        $this->health = $health;

        return $this;
    }

    /**
     * Get health
     *
     * @return integer
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * Set missileLevel
     *
     * @param integer $missileLevel
     *
     * @return MissileLevelList
     */
    public function setMissileLevel($missileLevel)
    {
        $this->missileLevel = $missileLevel;

        return $this;
    }

    /**
     * Get missileLevel
     *
     * @return integer
     */
    public function getMissileLevel()
    {
        return $this->missileLevel;
    }

    /**
     * Set labUnlockLevel
     *
     * @param integer $labUnlockLevel
     *
     * @return MissileLevelList
     */
    public function setLabUnlockLevel($labUnlockLevel)
    {
        $this->labUnlockLevel = $labUnlockLevel;

        return $this;
    }

    /**
     * Get labUnlockLevel
     *
     * @return integer
     */
    public function getLabUnlockLevel()
    {
        return $this->labUnlockLevel;
    }

    /**
     * Set ressource1Cost
     *
     * @param integer $ressource1Cost
     *
     * @return MissileLevelList
     */
    public function setRessource1Cost($ressource1Cost)
    {
        $this->ressource1Cost = $ressource1Cost;

        return $this;
    }

    /**
     * Get ressource1Cost
     *
     * @return integer
     */
    public function getRessource1Cost()
    {
        return $this->ressource1Cost;
    }

    /**
     * Set ressource2Cost
     *
     * @param integer $ressource2Cost
     *
     * @return MissileLevelList
     */
    public function setRessource2Cost($ressource2Cost)
    {
        $this->ressource2Cost = $ressource2Cost;

        return $this;
    }

    /**
     * Get ressource2Cost
     *
     * @return integer
     */
    public function getRessource2Cost()
    {
        return $this->ressource2Cost;
    }

    /**
     * Set ressource3Cost
     *
     * @param integer $ressource3Cost
     *
     * @return MissileLevelList
     */
    public function setRessource3Cost($ressource3Cost)
    {
        $this->ressource3Cost = $ressource3Cost;

        return $this;
    }

    /**
     * Get ressource3Cost
     *
     * @return integer
     */
    public function getRessource3Cost()
    {
        return $this->ressource3Cost;
    }

    /**
     * Set area
     *
     * @param float $area
     *
     * @return MissileLevelList
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return float
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set speed
     *
     * @param float $speed
     *
     * @return MissileLevelList
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return float
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set time
     *
     * @param float $time
     *
     * @return MissileLevelList
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return float
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set timeExplosion
     *
     * @param float $timeExplosion
     *
     * @return MissileLevelList
     */
    public function setTimeExplosion($timeExplosion)
    {
        $this->timeExplosion = $timeExplosion;

        return $this;
    }

    /**
     * Get timeExplosion
     *
     * @return float
     */
    public function getTimeExplosion()
    {
        return $this->timeExplosion;
    }

    /**
     * Set coolDown
     *
     * @param float $coolDown
     *
     * @return MissileLevelList
     */
    public function setCoolDown($coolDown)
    {
        $this->coolDown = $coolDown;

        return $this;
    }

    /**
     * Get coolDown
     *
     * @return float
     */
    public function getCoolDown()
    {
        return $this->coolDown;
    }

    /**
     * Get img
     *
     * @return text
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Get imgIcone
     *
     * @return text
     */
    public function getImgIcone()
    {
        return $this->imgIcone;
    }

    /**
     * Set freeze
     *
     * @param integer $freeze
     *
     * @return MissileLevelList
     */
    public function setFreeze($freeze)
    {
        $this->freeze = $freeze;

        return $this;
    }

    /**
     * Get freeze
     *
     * @return integer
     */
    public function getFreeze()
    {
        return $this->freeze;
    }

    /**
     * Set speedReduc
     *
     * @param float $speedReduc
     *
     * @return MissileLevelList
     */
    public function setSpeedReduc($speedReduc)
    {
        $this->speedReduc = $speedReduc;

        return $this;
    }

    /**
     * Get speedReduc
     *
     * @return float
     */
    public function getSpeedReduc()
    {
        return $this->speedReduc;
    }

    /**
     * Set magnet
     *
     * @param integer $magnet
     *
     * @return MissileLevelList
     */
    public function setMagnet($magnet)
    {
        $this->magnet = $magnet;

        return $this;
    }

    /**
     * Get magnet
     *
     * @return integer
     */
    public function getMagnet()
    {
        return $this->magnet;
    }

    /**
     * Set attractionRadius
     *
     * @param integer $attractionRadius
     *
     * @return MissileLevelList
     */
    public function setAttractionRadius($attractionRadius)
    {
        $this->attractionRadius = $attractionRadius;

        return $this;
    }

    /**
     * Get attractionRadius
     *
     * @return integer
     */
    public function getAttractionRadius()
    {
        return $this->attractionRadius;
    }

    /**
     * Set draggable
     *
     * @param integer $draggable
     *
     * @return MissileLevelList
     */
    public function setDraggable($draggable)
    {
        $this->draggable = $draggable;

        return $this;
    }

    /**
     * Get draggable
     *
     * @return integer
     */
    public function getDraggable()
    {
        return $this->draggable;
    }
}

