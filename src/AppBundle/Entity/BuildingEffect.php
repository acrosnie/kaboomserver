<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BuildingEffect
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class BuildingEffect
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Effect", type="string", length=60)
     */
    private $effect;

    /**
     * @var integer
     *
     * @ORM\Column(name="NumberAvailable", type="integer")
     */
    private $numberAvailable;

    /**
     * @var float
     *
     * @ORM\Column(name="Cooldown", type="float")
     */
    private $cooldown;

    /**
     * @var integer
     *
     * @ORM\Column(name="HealthAbility", type="integer")
     */
    private $healthAbility;

    /**
     * @var float
     *
     * @ORM\Column(name="Duration", type="float")
     */
    private $duration;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set effect
     *
     * @param string $effect
     *
     * @return BuildingEffect
     */
    public function setEffect($effect)
    {
        $this->effect = $effect;

        return $this;
    }

    /**
     * Get effect
     *
     * @return string
     */
    public function getEffect()
    {
        return $this->effect;
    }

    /**
     * Set numberAvailable
     *
     * @param integer $numberAvailable
     *
     * @return BuildingEffect
     */
    public function setNumberAvailable($numberAvailable)
    {
        $this->numberAvailable = $numberAvailable;

        return $this;
    }

    /**
     * Get numberAvailable
     *
     * @return integer
     */
    public function getNumberAvailable()
    {
        return $this->numberAvailable;
    }

    /**
     * Set cooldown
     *
     * @param float $cooldown
     *
     * @return BuildingEffect
     */
    public function setCooldown($cooldown)
    {
        $this->cooldown = $cooldown;

        return $this;
    }

    /**
     * Get cooldown
     *
     * @return float
     */
    public function getCooldown()
    {
        return $this->cooldown;
    }

    /**
     * Set healthAbility
     *
     * @param integer $healthAbility
     *
     * @return BuildingEffect
     */
    public function setHealthAbility($healthAbility)
    {
        $this->healthAbility = $healthAbility;

        return $this;
    }

    /**
     * Get healthAbility
     *
     * @return integer
     */
    public function getHealthAbility()
    {
        return $this->healthAbility;
    }

    /**
     * Set duration
     *
     * @param float $duration
     *
     * @return BuildingEffect
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return float
     */
    public function getDuration()
    {
        return $this->duration;
    }
}

