<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImgIcone
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ImgIcone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="randomId", type="integer")
     */
    private $randomId;

    /**
     * @var MissileLevelList $missileLevelList
     *
     * @ORM\ManyToOne(targetEntity="MissileLevelList", inversedBy="imgIcone", cascade={"persist", "merge"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $missileLevelList;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set randomId
     *
     * @param integer $randomId
     *
     * @return Img
     */
    public function setRandomId($randomId)
    {
        $this->randomId = $randomId;

        return $this;
    }

    /**
     * Get randomId
     *
     * @return integer
     */
    public function getRandomId()
    {
        return $this->randomId;
    }

    /**
     * Set name
     *
     * @param integer $name
     *
     * @return Img
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return integer
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param MissileLevelList $missileLevelList
     */

    public function setMissileLevelList(MissileLevelList $missileLevelList)
    {
        $this->missileLevelList = $missileLevelList;

        return $this;
    }

    /**
     * @return MissileLevelList $missileLevelList
     */
    public function getMissileLevelList()
    {
        return $this->missileLevelList;
    }
}

