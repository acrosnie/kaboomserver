<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Cache-Control" content="no-cache">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <title><?php $view['slots']->output('title', 'Home') ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo $view['assets']->getUrl('bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" />
    <link href="<?php echo $view['assets']->getUrl('bootstrap/css/bootstrap.css') ?>" rel="stylesheet" />
    <link href="<?php echo $view['assets']->getUrl('bootstrap/css/bootstrap-theme.min.css') ?>" rel="stylesheet" />
<style type="text/css">
    /* Some custom styles*/
    .demo-content{
        padding: 15px;
        font-size: 11px;
        min-height: 280px;
        background: #dbdfe5;
        margin-bottom: 10px;
        overflow: hidden;
    }
    .demo-content.bg-alt{
        background: #abb1b8;
    }
    .demo-content img.sprite
    {
      display: block;
      margin: 20px auto 0 auto;
    }
    .demo-content img.delete_img
    {
      width: 64px;
      height: 64px;
      margin-top: 16px;
      position: relative;
      left: 10%;
      display: inline;
      color: red;
    }
    .demo-content img.delete_img:hover
    {
      -webkit-filter: grayscale(100%);
      filter: grayscale(100%);
      -webkit-filter: contrast(20%);
      filter: contrast(20%);
      cursor : pointer;
    }
    .demo-content .number{
          font-weight: bold;
          font-size: 20px;
          text-align: left;
          background-color: #fff;
          padding: 5px;
          border-radius: 30px;

    }
    .demo-content .name-sprite{
      font-size: 8px;
      margin-left: 10px;
    }
    .demo-content .fileUpload {
        position: relative;
        overflow: hidden;
        padding: 10px;
        font-size: 20px;
        top: 7.5px;
        left: 25px;
    }
    .demo-content .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
</style>
    <?php $view['slots']->output('otherCss', '') ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body role="document">
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $view['router']->generate('home')?>">KABOOM</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo $view['router']->generate('home')?>">Home</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CSV UPLOAD<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li>---------------------- BUILDING</li>
                <li><a href="<?php echo $view['router']->generate('csv_building_list')?>">CSV ADD BUILDING LIST</a></li>
                <li><a href="<?php echo $view['router']->generate('csv_level_building_max')?>">CSV ADD LEVEL BUILDING MAX</a></li>
                <li><a href="<?php echo $view['router']->generate('csv_building_level_list_info')?>">CSV ADD BUILDING LEVEL LIST INFO</a></li>
                <li><a href="<?php echo $view['router']->generate('csv_building_effect_list')?>">CSV ADD BUILDING EFFECT LIST</a></li>
                <li>---------------------- MISSILE</li>
                <li><a href="<?php echo $view['router']->generate('csv_missile_list')?>">CSV ADD MISSILE LIST</a></li>
                <li><a href="<?php echo $view['router']->generate('csv_missile_level_list_info')?>">CSV ADD MISSILE LEVEL LIST</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">API GET<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li>---------------------- BUILDING</li>
                <li><a href="<?php echo $view['router']->generate('api_get_building_list')?>">GET BUILDING LIST</a></li>
                <li><a href="<?php echo $view['router']->generate('api_get_building_player_list', array('playerId' => '1'))?>">GET BUILDING PLAYER LIST</a></li>
                <li><a href="<?php echo $view['router']->generate('api_get_building_level_list')?>">GET BUILDING LEVEL LIST</a></li>
                <li><a href="<?php echo $view['router']->generate('api_get_building_effect_list')?>">GET BUILDING EFFECT LIST</a></li>
                <li>---------------------- MISSILE</li>
                <li><a href="<?php echo $view['router']->generate('api_get_missile_list')?>">GET MISSILE LIST</a></li>
                <li><a href="<?php echo $view['router']->generate('api_get_missile_player_list', array('playerId' => '1'))?>">GET MISSILE PLAYER LIST</a></li>
                <li><a href="<?php echo $view['router']->generate('api_get_missile_level_list')?>">GET MISSILE LEVEL LIST</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SPRITE<span class="caret"></span></a>
              <ul class="dropdown-menu">
            <li><a href="<?php echo $view['router']->generate('add_img_building')?>">ADD SPRITE BUILDING</a></li>
            <li><a href="<?php echo $view['router']->generate('add_img_missile')?>">ADD SPRITE MISSILE</a></li>
            <li><a href="<?php echo $view['router']->generate('add_img_missile_icone')?>">ADD SPRITE MISSILE ICONE</a></li>
            </li>
          </ul>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ADD / EDIT INFO<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li>---------------------- BUILDING</li>
                  <li><a href="<?php echo $view['router']->generate('edit_add_building_player')?>">ADD BUILDING PLAYER</a></li>
                  <li><a href="<?php echo $view['router']->generate('edit_edit_building_player')?>">EDIT BUILDING PLAYER</a></li>
                <li>---------------------- MISSILE</li>
                  <li><a href="<?php echo $view['router']->generate('edit_add_missile_player')?>">ADD MISSILE PLAYER</a></li>
                  <li><a href="<?php echo $view['router']->generate('edit_edit_missile_player')?>">EDIT MISSILE PLAYER</a></li>
              </ul>
            </li>
        </div>
      </div>
    </nav>
    <div class="container theme-showcase" role="main">
        <div class="jumbotron" style="margin-top: 100px;">
               <h1><img src="<?php echo $view['assets']->getUrl('img/bill.png') ?>" height="64"><?php $view['slots']->output('titreH1', 'Kaboom Home') ?></h1>
        </div>
    <div class="page-header"></div>
      <?php $view['slots']->output('body') ?>
      <div class="page-header"></div>
    </div>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('bootstrap/js/bootstrap.min.js') ?>"></script>
    <?php $view['slots']->output('otherScript', '') ?>
  </body>
</html>