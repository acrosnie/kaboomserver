<!-- app/Resources/views/csvLevelBuildingMaxAction.html.php -->
<?php $view->extend('views/base.html.php') ?>

<?php $view['slots']->set('title', 'Update Csv Building Effect List') ?>

<?php $view['slots']->set('titreH1', 'Update Csv Building Effect List') ?>

<?php $view['slots']->start('body') ?>
<form method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input type="file" id="exampleInputFile" name="csvFile">
    <p class="help-block">File.csv</p>
  </div>
  <button type="submit" class="btn btn-default" name="csvPost">Submit</button>
</form>
<?php 
if ($success)
{
	echo '<div class="page-header"></div><div class="alert alert-success" role="alert">DataBase has been uploaded with success :) !</div>';
}
if ($error)
{
	echo '<div class="page-header"></div><div class="alert alert-danger" role="alert">Oh snap! Change a few things up and try submitting again.</div>';
}
?>
<?php $view['slots']->stop() ?>


