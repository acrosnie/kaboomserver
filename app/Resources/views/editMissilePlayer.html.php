<!-- app/Resources/views/csvLevelBuildingMaxAction.html.php -->
<?php
$selectArray = array();
$selectPlayer = "";
foreach ($playerList as $key => $value)
{
	$selectArray[] = '<option value="'.$value->getId().'">'.$value->getPseudo().'</option>';
}
sort($selectArray);
foreach ($selectArray as $key => $value)
{
	$selectPlayer .= $value;
}
?>
<?php $view->extend('views/base.html.php') ?>
<?php $view['slots']->set('title', 'Edit Missile Player') ?>
<?php $view['slots']->set('titreH1', 'Edit Missile Player') ?>
<?php $view['slots']->start('body') ?>
<div style="display:none;" class="notif">
	<div class="btn btn-success" style="margin:0 auto;margin-top:40px;display:block;"><span>Missile Edit with success !</span></div>
</div>
<div class="loader" style="display:none;"><img src="<?php echo $view['assets']->getUrl('img/ajax-loader.gif') ?>" height="24" style="margin: 0 auto;display: block; margin-top: 30px;"></div>
<form>
	<div class="form-group">
		<label for="selectPlayer">Select Player</label>
		<select class="form-control" name="selectPlayer" id="selectPlayer">
		<?php echo $selectPlayer; ?>
		</select>
	</div>
</form>

<div id="missilePlayerContain">
	

</div>
<?php $view['slots']->stop() ?>
<?php $view['slots']->start('otherScript') ?>
<script type="text/javascript">
$('#selectPlayer').on("change", function (e)
{
	e.preventDefault();
	$("#missilePlayerContain").empty();
	ajaxLoadMissilePlayerEdit();
});

$('body').on("click", '.edit', function (e)
{
	$(".loader").show();
	var url = <?php echo '"'.$view['router']->generate('edit_edit_missile_player_value', array('idMissilePlayer' => 0, 'idMissileLevelList' => 0, 'nbr' => 0, 'nbrEquiped' => 0)).'"'; ?>;
	var idMissilePlayer = $(this).parent().parent().find('.idMissilePlayer').text();
	var idMissileLevelList = $(this).parent().find("#selectMissileLevelList").val();
	var nbr = $(this).parent().find("#nbr").val();
	var nbrEquiped = $(this).parent().find("#nbrEquiped").val();
	var res = url.substr(0, url.length - 7) + idMissilePlayer + "/" + idMissileLevelList + "/" + nbr + "/" + nbrEquiped;
	$.ajax({
		url: res,
		dataType: 'text',
		cache: false,
		type: 'post',
		async: true,
		success: function(php_script_response)
		{
			$(".loader").hide();
			$(".notif").show().delay(800).slideUp(400);
			console.log(php_script_response);
		},
		error:function()
		{
			alert("Error in Edit Missile");
			$(".loader").hide();
		}
	});
});

$('body').on("click", '.delete', function (e)
{
    var url = <?php echo '"'.$view['router']->generate('edit_delete_missile_player', array('idMissilePlayer' => 0)).'"'; ?>;  
    var id = $(this).parent().parent().find('.idMissilePlayer').text();
    var res = url.substr(0, url.length - 1) + id;
    console.log(res);
    $(this).parent().parent().remove();        
    $.ajax({
        url: res, // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,                        
        type: 'post',
        async: false,
        success: function(php_script_response)
        {
          console.log("missile player supprime !");
          $(this).find(".dropzoneAuto").remove();
          $(this).parent().parent().remove();
        },
        complete: function(php_script_response)
        {
        $(this).parent().parent().remove();
        var i = 1;
        // $( ".dropzoneAuto" ).each(function(index)
        // {
        //   var number = $(this).find('.number').text();
        //   $(this).find('.number').html(i);
        //   var name = $(this).find('.name-sprite').text();
        //   var res = name.substr(0, name.length - 4 - number.length) + i + ".png";
        //   $(this).find('.name-sprite').html(res);
        //   i++;
        // });
        },
        error:function(error)
        {
          console.log("error in delete : " + error);
        }
     });
    return (false);
  
});

function ajaxLoadMissilePlayerEdit()
{
	$( ".loader").show();
	$( ".apercu_img").empty();
	var url = <?php echo '"'.$view['router']->generate('edit_get_building_player_html', array('idPlayer' => 0)).'"'; ?>;
	var id = $("#selectPlayer").val();
	var res = url.substr(0, url.length - 1) + id;
	$.ajax({
	type:"GET",
	url : res,
	async: false,
	cache: false,
	success : function(response)
	{
		$("#missilePlayerContain").html(response);
		$( ".loader").hide();
	},
	error: function(error)
	{
		console.log("error ajax send: " + error);
		$( ".loader").hide();
	}
	});
}
ajaxLoadMissilePlayerEdit();
</script>
<?php $view['slots']->stop() ?>