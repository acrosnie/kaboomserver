<!-- app/Resources/views/csvLevelBuildingMaxAction.html.php -->
<?php
$selectArray = array();
$select = "";
foreach ($buildingLevelList as $key => $value)
{
	$selectArray[] = '<option nameBuilding="'.$value->getBuildingList()->getName().' - Lvl '.$value->getLevel().'" value="'.$value->getId().'">'.$value->getBuildingList()->getName().' - Lvl '.$value->getLevel().'</option>';
}
sort($selectArray);
foreach ($selectArray as $key => $value)
{
	$select .= $value;
}
?>
<?php $view->extend('views/base.html.php') ?>
<?php $view['slots']->set('otherCss', '<link href="'.$view['assets']->getUrl('css/dropzone.css').'" rel="stylesheet"/>') ?>
<?php $view['slots']->set('title', 'Upload Edit Img Sprite') ?>
<?php $view['slots']->set('titreH1', 'Upload Edit Img Sprite') ?>
<?php $view['slots']->start('body') ?>
<form action="#" method="POST">
	<div class="form-group">
		<label for="selectBuilding">Select your Building</label>
		<select class="form-control" name="buildingLevelListForm" id="selectBox">
		<?php echo $select; ?>	
		</select>
	</div>
<!--  <div class="fallback">
    <input name="file" type="file" acceptedFiles="image/*" multiple />
  </div> -->
</form>
<div class="apercu_img"></div>
<!-- <img data-src="holder.js/200x200" class="img-thumbnail" alt="200x200" style="width: 200px; height: 200px;" src="" data-holder-rendered="true"> -->
<?php $view['slots']->stop() ?>
<?php $view['slots']->start('otherScript') ?>
<script type="text/javascript" src=""<?php echo $view['assets']->getUrl('js/dropzone.js'); ?>""></script>
<script type="text/javascript">
// Dropzone.options.myAwesomeDropzone = {
//   paramName: "file", // The name that will be used to transfer the file
//   maxFilesize: 2, // MB
//   acceptedFiles: "image/*",
//   dictDefaultMessage : "Drag And Drop Or Click Here For Upload Sprites.",
//   accept: function(file, done) {
//     if (file.name == "justinbieber.jpg") {
//       done("Naha, you don't.");
//     }
//     else
//     {
//     	done();
//     }
//   },
// 	success: function(file, response){
// 	    ajaxSend();
// 	}
// };
$('#selectBox').change(function()
{
	//$(".dz-preview").remove();
	ajaxSend();
});
function ajaxSend()
{
	$( ".apercu_img" ).empty();
	var urlDrop = <?php echo '"'.$view['router']->generate('edit_img_uploadFile').'"'; ?>;
	var url = <?php echo '"'.$view['router']->generate('api_get_sprite', array('idBuildingLevelList' => 0)).'"'; ?>;
	var id = $("#selectBox").val();
	var res = url.substr(0, url.length - 1) + id;
    $.ajax({
        type:"GET",
        url : res,
        async: false,
        success : function(response)
        {
        	$( ".apercu_img" ).empty().append(response);
        	//var myDropzone = new Dropzone("div#dropzoneAuto", { url: "/file/post"});
        	//$("div#dropzoneAuto").dropzone({ url: urlDrop });
        },
        error: function() {
            alert('Error occured');
        }
    });
}
ajaxSend();
</script>
<?php $view['slots']->stop() ?>