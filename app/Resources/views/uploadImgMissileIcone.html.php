<!-- app/Resources/views/csvLevelBuildingMaxAction.html.php -->
<?php
$selectArray = array();
$select = "";
foreach ($missileLevelList as $key => $value)
{
  //print_r($value);
  if ($value->getMissileList() != null)
    $selectArray[] = '<option nameMissile="'.$value->getMissileList()->getName().' - Lvl '.$value->getMissileLevel().'" value="'.$value->getId().'">'.$value->getMissileList()->getName().' - Lvl '.$value->getMissileLevel().'</option>';
}
sort($selectArray);
foreach ($selectArray as $key => $value)
{
	$select .= $value;
}
?>
<?php $view->extend('views/base.html.php') ?>
<?php $view['slots']->set('otherCss', '<link href="'.$view['assets']->getUrl('css/dropzone.css').'" rel="stylesheet"/>') ?>
<?php $view['slots']->set('title', 'Upload Img Missile Icone') ?>
<?php $view['slots']->set('titreH1', 'Upload Img Missile Sprite Icone') ?>
<?php $view['slots']->start('body') ?>
<form action="<?php echo $view['router']->generate('add_img_uploadFileMissileIcone')?>" class="dropzone" id="myAwesomeDropzone" method="POST">
	<div class="form-group">
		<label for="selectMissile">Select your Missile</label>
		<select class="form-control" name="missileLevelListForm" id="selectBox">
		<?php echo $select; ?>	
		</select>
	</div>
 <div class="fallback">
    <input name="file" type="file" acceptedFiles="image/*" multiple />
  </div>
</form>
<div class="loader"><img src="<?php echo $view['assets']->getUrl('img/ajax-loader.gif') ?>" height="24" style="margin: 0 auto;display: block; margin-top: 30px;"></div>
<div class="apercu_img"></div>
<!-- <img data-src="holder.js/200x200" class="img-thumbnail" alt="200x200" style="width: 200px; height: 200px;" src="" data-holder-rendered="true"> -->
<?php $view['slots']->stop() ?>













<?php $view['slots']->start('otherScript') ?>
<script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/dropzone.js'); ?>"></script>
<script type="text/javascript">
Dropzone.options.myAwesomeDropzone = {
  uploadMultiple : true,
  paramName: "file", // The name that will be used to transfer the file
  maxFilesize: 15, // MB
  acceptedFiles: "image/*",
  dictDefaultMessage : "Drag And Drop Or Click Here For Upload Sprites.",
  accept: function(file, done) {
    if (file.name == "justinbieber.jpg") {
      done("Naha, you don't.");
    }
    else
    {
    	done();
    }
  },
	successmultiple: function(file, response){
	    ajaxSend();
      console.log(response);
	},
  error:function(file, error){
    console.log(error);
  }
};
$('#selectBox').on("change", function (e)
{
  e.preventDefault();
	$(".dz-preview").remove();
	ajaxSend();
});


function ajaxSend()
{
  $( ".loader").show();
  $( ".apercu_img").empty();
  var url = <?php echo '"'.$view['router']->generate('api_get_sprite_missile_icone', array('idMissileLevelList' => 0)).'"'; ?>;
  var id = $("#selectBox").val();
  var res = url.substr(0, url.length - 1) + id;
  $.ajax({
      type:"GET",
      url : res,
      async: false,
      cache: false,
      success : function(response)
      {
      	$(".apercu_img").html(response);
        $( ".loader").hide();
      },
      error: function(error) {
          console.log("error ajax send: " + error);
          $( ".loader").hide();
      }
  });
}

$('body').on("click", '.delete_img', function (e)
{
    //event.preventDefault();
    var name = $(this).parent().find('.name-sprite').text();
    var url = <?php echo '"'.$view['router']->generate('delete_img_uploadFileMissileIcone', array('idImg' => 0, 'imageName' => "i")).'"'; ?>;  
    var id = $(this).parent().parent().find('.id-sprite').text();
    var res = url.substr(0, url.length - 3) + id;
    res = res + "/" + name;
    $(this).parent().parent().remove();        
    $.ajax({
        url: res, // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,                        
        type: 'post',
        async: false,
        success: function(php_script_response)
        {
          console.log("image supprime !");
          $(this).find(".dropzoneAuto").remove();
          $(this).parent().parent().remove();
        },
        complete: function(php_script_response)
        {
        $(this).parent().parent().remove();
        var i = 1;
        $( ".dropzoneAuto" ).each(function(index)
        {
          var number = $(this).find('.number').text();
          $(this).find('.number').html(i);
          var name = $(this).find('.name-sprite').text();
          var res = name.substr(0, name.length - 4 - number.length) + i + ".png";
          $(this).find('.name-sprite').html(res);
          i++;
        });
        },
        error:function(error)
        {
          console.log("error in delete : " + error);
        }
     });
    return (false);
  
});
ajaxSend();
</script>
<?php $view['slots']->stop() ?>