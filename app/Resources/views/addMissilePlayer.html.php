<!-- app/Resources/views/csvLevelBuildingMaxAction.html.php -->
<?php
$selectArray = array();
$selectPlayer = "";
foreach ($playerList as $key => $value)
{
	$selectArray[] = '<option value="'.$value->getId().'">'.$value->getPseudo().'</option>';
}
sort($selectArray);
foreach ($selectArray as $key => $value)
{
	$selectPlayer .= $value;
}
$selectArray = array();
$selectMissileLevelList = "";
foreach ($missileLevelList as $key => $value)
{
	$selectArray[] = '<option value="'.$value->getId().'">'.$value->getMissileList()->getName().' - Level '.$value->getMissileLevel().'</option>';
}
sort($selectArray);
foreach ($selectArray as $key => $value)
{
	$selectMissileLevelList .= $value;
}
?>
<?php $view->extend('views/base.html.php') ?>
<?php $view['slots']->set('title', 'Add Missile Player') ?>
<?php $view['slots']->set('titreH1', 'Add Missile Player') ?>
<?php $view['slots']->start('body') ?>
<div style="display:none;" class="notif">
	<div class="btn btn-success" style="margin:0 auto;margin-top:40px;display:block;"><span>Missile add with success !</span></div>
</div>
<form>
	<div class="form-group">
		<label for="selectPlayer">Select Player</label>
		<select class="form-control" name="selectPlayer" id="selectPlayer">
		<?php echo $selectPlayer; ?>
		</select>
	</div>
	<div class="form-group">
		<label for="selectPlayer">Select Missile</label>
		<select class="form-control" name="selectMissileLevelList" id="selectMissileLevelList">
			<?php echo $selectMissileLevelList; ?>
		</select>
	</div>
	<div class="addMissile btn btn-success" style="margin:0 auto;margin-top:40px;display:block;"><span>Add Missile</span></div>
</form>
<div class="loader" style="display:none;"><img src="<?php echo $view['assets']->getUrl('img/ajax-loader.gif') ?>" height="24" style="margin: 0 auto;display: block; margin-top: 30px;"></div>
<?php $view['slots']->stop() ?>
<?php $view['slots']->start('otherScript') ?>
<script type="text/javascript">
$('body').on("click", '.addMissile', function (e)
{
	$(".addMissile").slideUp(100);
	console.log("Hello");
  	$(".loader").show();
    var url = <?php echo '"'.$view['router']->generate('api_add_missile_player', array('playerId' => 0, 'missileLevelListId' => 0)).'"'; ?>;  
    var playerId = $("#selectPlayer").val();
    var missileLevelListId = $("#selectMissileLevelList").val();
    var res = url.substr(0, url.length - 3) + playerId + "/" + missileLevelListId;
    console.log(res);   
    $.ajax({
        url: res, // point to server-side PHP script 
        dataType: 'text',  // what to expect back from the PHP script, if anything
        cache: false,                      
        type: 'post',
        async: true,
        success: function(php_script_response)
        {
            $(".loader").hide();
            $(".notif").show().delay(800).slideUp(400);
            $(".addMissile").show(400);
        },
        error:function()
        {
          alert("Error in Add img");
          $(".loader").hide();
        }
     });
});
</script>
<?php $view['slots']->stop() ?>